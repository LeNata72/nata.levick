﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoysticPlayer : MonoBehaviour
{
    public float speed = 1;
    public Vector3 move;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    
    void FixedUpdate()
    {
        rb.velocity = move * speed * Time.deltaTime;
    }
}
