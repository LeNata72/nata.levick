﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoysticControl : MonoBehaviour
{
    public Transform joy;
    public JoysticPlayer player;
    Vector3 targetVector, touchPos;


    void Start()
    {
        joy.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount>0)
        {
            touchPos = Input.GetTouch(0).position;
            targetVector = touchPos - transform.position;
            if (targetVector.magnitude<100)
            {
                joy.position = touchPos;
                player.move = targetVector;
            }
        }
        else
        {
            joy.position = transform.position;
            player.move = Vector3.zero;
        }
    }
}
