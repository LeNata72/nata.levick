﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidDem : MonoBehaviour
{
    public Transform rootCam, obgCam;
    public float sensetyCamera = 10;
    Transform cam;
    Rigidbody rb;
    Vector3 accel;
    Vector3 fromCam = new Vector3(0, 1, -3);
    Vector3 toCam = new Vector3(0, 3.7f, -8);
    float dis1, dis2, newProc, currentProc = 0.5f;
    
    void Start()
    {
        cam = Camera.main.transform;
        rb = GetComponent<Rigidbody>();
        cam.localPosition = Vector3.Lerp(fromCam, toCam, currentProc);
    }

    
    void Update()
    {
        //print(Input.acceleration);
        // print(Input.deviceOrientation.ToString());
        /* print(Input.touchCount);
         if (Input.touchCount>0)
         {
             print(Input.GetTouch(0).position);
             print(Input.GetTouch(0).deltaPosition);
             print(Input.GetTouch(0).deltaTime);
         }
             print(Input.GetTouch(0).phase);
        */

        rootCam.position = transform.position;
        accel = Input.acceleration;
        rb.velocity = rootCam.TransformDirection(accel.x * sensetyCamera, rb.velocity.y, (accel.y + 0.6f) * sensetyCamera);

        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase==TouchPhase.Moved)
            {
                Vector2 move = Input.GetTouch(0).deltaPosition;
                if (Mathf.Abs(move.x) > 0.4f)
                {
                    rootCam.Rotate(0, move.x, 0, Space.World);
                }
                if (Mathf.Abs(move.y)>0.4f)
                {
                   /* Vector3 rot = obgCam.localEulerAngles;
                    obgCam.Rotate(move.y*0.2f, 0, 0, Space.Self);
                   
                    if ((obgCam.localEulerAngles.x > 350 && obgCam.localEulerAngles.x < 360) || (obgCam.localEulerAngles.x < 40 && obgCam.localEulerAngles.x > 0))
                    {
                       // obgCam.localEulerAngles = rot;
                    }
                    else
                    {
                        obgCam.localEulerAngles = rot;
                    }*/
                    float rotateX = obgCam.localEulerAngles.x + move.y;
                    if ((rotateX >-10 && rotateX < 40) || (rotateX > 350 && rotateX < 400))
                    {
                        obgCam.Rotate(move.y, 0, 0, Space.Self);

                    }
                    print(rotateX);
                }
            }
        }

        else if (Input.touchCount==2)
        {
            if (Input.GetTouch(0).phase==TouchPhase.Began || Input.GetTouch(1).phase==TouchPhase.Began)
            {
                dis1 = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved) 
            {
                dis2 = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
                if (dis1 > dis2)
                {
                    float proc = 1f - dis2 / (dis1 / 100f) * 0.01f;//dis1=50 //dis2 = 40 //50:100 =0.5(1%)
                    //40:05 = 80  * 0.01 = 0.8%
                    //1-0.8 = 0.2
                    newProc = currentProc + proc;
                }
                else //dis1<dis2
                {
                    float proc = 1f - dis1 / (dis2 / 100f) * 0.01f;
                    newProc = currentProc - proc;
                }
                newProc = Mathf.Clamp01(newProc);

                if (Mathf.Abs(currentProc-newProc)>0.02f)
                {
                    if (currentProc<newProc)
                    {
                        currentProc += 0.2f;
                    }
                    else 
                    {
                        currentProc -= 0.02f;   
                    }
                    cam.localPosition = Vector3.Lerp(fromCam, toCam, currentProc);
                }
            }
        }

    }

    private void OnMouseDown()
    {
        if (Physics.Raycast(transform.position,Vector3.down, 0.6f))
        {          
            rb.AddForce(0, 300, 0);
        }
    }
}
