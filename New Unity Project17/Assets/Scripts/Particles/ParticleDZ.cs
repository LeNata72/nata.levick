﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleDZ : MonoBehaviour
{
    ParticleSystem ps;
    List<ParticleSystem.Particle> exitPar = new List<ParticleSystem.Particle>();
    List<ParticleSystem.Particle> enterPar = new List<ParticleSystem.Particle>();



    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
    }


    private void OnParticleTrigger()
    {
        int countParE = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterPar);
        int countPar = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Exit, exitPar);
        for (int i = 0; i < countParE; i++)
        {
            ParticleSystem.Particle tempPar = enterPar[i];
            tempPar.startColor = Color.red;
            enterPar[i] = tempPar;
        }
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterPar);

        for (int i = 0; i < countPar; i++)
        {
            ParticleSystem.Particle tempPar = exitPar[i];
            tempPar.startColor = Color.green;
            exitPar[i] = tempPar;
        }
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Exit, exitPar);
    }

}
