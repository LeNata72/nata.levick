﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticelCollisionDZ : MonoBehaviour
{
        int cubeHP = 0;

    private void Start()
    {
        cubeHP = 3;
    }
     public Transform cubeEffect;
    private void OnCollisionEnter(Collision collision)
    {
        
             

        if (collision.transform.tag == "Bonus")
        {
           cubeHP--;
             if (cubeHP == 0)
             {
                   Destroy(collision.gameObject);
                   Instantiate(cubeEffect, collision.contacts[0].point, Quaternion.identity);

             }
    
        }
             
        
    }
}
