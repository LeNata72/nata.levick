﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEmit : MonoBehaviour
{
    public Transform particleEmit;

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.transform.tag == "Bonus")
        {
            Instantiate(particleEmit, collision.contacts[0].point, Quaternion.identity);
        }
    }
}
