﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleExsplosion : MonoBehaviour
{
    ParticleSystem ps;
    List<ParticleCollisionEvent> collEvent = new List<ParticleCollisionEvent>();
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other)//куда врезался партикл
    {
        if (other.tag == "Blocks")
        {
            int countColl = ps.GetCollisionEvents(other, collEvent);
            Rigidbody rb = other.GetComponent<Rigidbody>();
            foreach (var item in collEvent)
            {
                rb.AddForce(item.velocity * 100);
            }
        }
    }

}
