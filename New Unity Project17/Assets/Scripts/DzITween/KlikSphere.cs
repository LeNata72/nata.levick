﻿using UnityEngine;

public class KlikSphere : MonoBehaviour
{
    Transform obj1, obj2;
    
    void Start()
    {
        for (int x = 0; x < 5; x++)
        {
            for (int y = 0; y < 5; y++)
            {
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                obj.transform.position = new Vector3(x, y, 0);
                obj.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                obj.transform.parent = transform;
            }
        }
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray,out hit))
            {
                if (!obj1)
                {
                    obj1 = hit.transform;
                }
                else
                {
                    obj2 = hit.transform;
                    if(obj1!=obj2)
                    
                    {
                        iTween.MoveTo(obj1.gameObject, obj2.position, 1);
                        iTween.MoveTo(obj2.gameObject, obj1.position, 1);
                    }
                        obj1 = null;
                        obj2 = null;
                }

            }
        }
    }
}
