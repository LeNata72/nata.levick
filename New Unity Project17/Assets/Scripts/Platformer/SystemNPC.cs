﻿using UnityEngine;

public class SystemNPC : MonoBehaviour
{
    public static SystemNPC sing;
    public Transform[] pointsRespawn;
    public LayerMask layerSensorSpawn;
    private NPCPlatformer npc;
    private NPCPlatformer2 npc2;
    int countNPC = 0;
    private void Awake()
    {
        sing = this;

        npc = Resources.Load<NPCPlatformer>("NPC1");
        npc2 = Resources.Load<NPCPlatformer2>("NPC2");

    }
    public void AddCountNPC()
    {
        countNPC++;
    }

    public void RemoveNPC()
    {
        countNPC--;
        if(countNPC <= 2)
        {
            int randomPoint = Random.Range(0, pointsRespawn.Length);
            for (int i = 0; i < pointsRespawn.Length; i++)
            {

            if (!Physics2D.OverlapCircle(pointsRespawn[randomPoint].position, 2, layerSensorSpawn))
            {
                if(Random.Range(0,9)%2 == 0)
                {
                    Instantiate(npc, pointsRespawn[randomPoint].position, Quaternion.identity, transform);
                }
                else
                {
                    Instantiate(npc, pointsRespawn[randomPoint].position, Quaternion.identity, transform);
                }
                    return;
            }
            else
                {
                    randomPoint = ++randomPoint % pointsRespawn.Length;
                    //3%4=3  4%4 =0
                }
            }
            Debug.LogError("Not point respawn of NPC");
        }
    }


}
