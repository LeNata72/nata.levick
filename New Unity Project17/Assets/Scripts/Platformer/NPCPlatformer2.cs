﻿using System.Collections;
using UnityEngine;

public class NPCPlatformer2 : MonoBehaviour
{
    public float speed = 1;        // скорость нпс
    public LayerMask layerGround;  // слой на кот находится земля
    public Rigidbody2D bullet;     // префаб пули
    public Transform sensorForward, sensorUp, sensorDown;
    Animator anim;                 // ссылка на компонент Animator 
    Rigidbody2D rb;                // ссылка на компонент Rigidbody2D 
    SpriteRenderer sr;             // ссылка на  SpriteRenderer 
    float move = 1;                  // напрямок руху 1 - вправо, -1 - вліво
    bool isRight = true;           // напрямок руху true - вправо, false - вліво
    Transform playerTrans;         // посилання на Transform персонажа
    float timerBulet = 0.5f;       // таймер для стрільби
    bool isShot = true;            // ознака того, що можна стріляти
    bool isJamp = false;
    bool isGround = false;

    // инициализация ссылок на компоненты, установка начальных параметров, запуск корутины
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim.SetBool("ground", true);
        playerTrans = PlayerControllerPlat.sing.transform;
        StartCoroutine(TimeShot());
        SystemNPC.sing.AddCountNPC();
    }
 

    // перевірка на те чи не закінчилась платформа, якщо так то НПС повертається та йде в інший бік
    void FixedUpdate()
    {
        anim.SetFloat("ySpeed", rb.velocity.y);

        if (!isJamp)
        {
            isGround = Physics2D.OverlapCircle(transform.position + new Vector3(0, -0.8f, 0), 0.1f, layerGround);
            anim.SetBool("ground", isGround);

            if (isGround)
            {
                if (isRight && !Physics2D.OverlapCircle(transform.position + new Vector3(0.8f, -0.8f, 0), 0.1f, layerGround))
                {
                    ChoiceDirection();
                }
                else if (!isRight && !Physics2D.OverlapCircle(transform.position + new Vector3(-0.8f, -0.8f, 0), 0.1f, layerGround))
                {
                    ChoiceDirection();
                }
                rb.velocity = new Vector2(move * speed, rb.velocity.y);
            }
        }
    }
    // реалізація стрільби НПС з перевіркою на те, де знаходиться персонаж, якщо перед ним, то стріляє з інтервалом
    private void Update()
    {
        if (isShot && Vector3.Distance(transform.position, playerTrans.position) < 7)
        {
            if (Mathf.Abs(transform.position.y - playerTrans.position.y) < 1)
            {
                if (isRight && transform.position.x - playerTrans.position.x < 0)
                {
                    // Rigidbody2D newBullet = Instantiate(bullet, transform.position + new Vector3(0.4f * move, 0, 0), Quaternion.identity);

                    Rigidbody2D newBullet = PoolSys.sys.TakeFromPool(1, transform.position + new Vector3(0.4f * move, 0, 0)).GetComponent<Rigidbody2D>();
                    newBullet.AddForce(new Vector2(500, 0));
                }
                else if (!isRight && transform.position.x - playerTrans.position.x > 0)
                {
                    //Rigidbody2D newBullet = Instantiate(bullet, transform.position + new Vector3(0.4f * move, 0, 0), Quaternion.identity);

                    Rigidbody2D newBullet = PoolSys.sys.TakeFromPool(1, transform.position + new Vector3(0.4f * move, 0, 0)).GetComponent<Rigidbody2D>();
                    newBullet.AddForce(new Vector2(-500, 0));
                }
                isShot = false;
                timerBulet = 0.5f;
            }
        }
    }

    // корутина, що виконує роль таймера, забезпечує проміжок в часі між вистрілами
    IEnumerator TimeShot()
    {
        while (true)
        {
            yield return null;
            timerBulet -= Time.deltaTime;
            if (timerBulet < 0)
            {
                isShot = true;
            }
        }
    }

    IEnumerator TimeJamp()
    {
        yield return new WaitForSeconds(0.6f);
        isJamp = false;
    }
    // якщо виникає колізія НПС з кулею персонажа, то видаляємо об'єкт НПС
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            SystemNPC.sing.RemoveNPC();
            Destroy(gameObject);
        }
    }

    void ChoiceDirection()
    {
        if (Physics2D.OverlapCircle(sensorForward.position, 0.1f, layerGround) ||
           Physics2D.OverlapCircle(sensorUp.position, 0.1f, layerGround))
        {
            Jamp();
        }
        else if (Physics2D.OverlapCircle(sensorDown.position, 0.1f, layerGround))
        {
        }
        else
        {
            Flip();
        }
    }

    void Jamp()
    {
        rb.AddForce(new Vector2(0, 350));
        anim.SetBool("ground", false);
        isJamp = true;
        StartCoroutine(TimeJamp());
    }

    void Flip()
    {
        if (isRight)
        {
            isRight = false;
            sr.flipX = true;
            move = -1;
        }
        else if (!isRight)
        {
            isRight = true;
            sr.flipX = false;
            move = 1;
        }
        sensorForward.localPosition = Vector3.Scale(sensorForward.localPosition, new Vector3(-1, 1, 1));
        sensorDown.localPosition = Vector3.Scale(sensorDown.localPosition, new Vector3(-1, 1, 1));
        sensorUp.localPosition = Vector3.Scale(sensorUp.localPosition, new Vector3(-1, 1, 1));
    }
}