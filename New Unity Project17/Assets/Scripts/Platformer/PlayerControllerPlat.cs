﻿using UnityEngine;

public class PlayerControllerPlat : MonoBehaviour
{
    public float speed;//скорость движения
    public LayerMask layerGround;//слой кот отвечает за...31 строка
    public Rigidbody2D bullet;//пуля
    public int countDiamond = 0;//количество алмазов
    public int hp = 30;//жизни
    public static PlayerControllerPlat sing;//переменная для образования синглетона
    Animator anim;//dostyp k animatory
    Rigidbody2D rb;//dostyp k RB
    SpriteRenderer sr;//доступ до компонента спрайт рендерер ссылка на компонент
    float move;//направл движения 1 - право, -1 лево
    bool isRight = true; //правда, так как по умолчанию смотрит вправо
    bool isGraund = false;//переменная для проверки в фиксед апдейте находится ли персонаж на земле

    ParallaxPlatformer par;
    Transform cam;
    Vector3 deltaPosCam;
    Vector3 newPosCam;
    float posX, posY;

    //public GUIParallax gui;
    public UIParallax ui;


    private void Awake()
    {
        sing = this;
    }

    //в старте выполняем инициализацию переменных
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        cam = Camera.main.transform;
        par = cam.GetComponentInChildren<ParallaxPlatformer>();
        deltaPosCam = cam.position - transform.position;
        posX = transform.position.x;
        posY = transform.position.y;

        // gui.InitGUI(hp);
        ui.InitUI(hp);
    }
    private void FixedUpdate()//перемещение по осям. То что возвращ метод сохраняем в переменной выше 9 строка
    {
        isGraund = Physics2D.OverlapCircle(transform.position + new Vector3(0, -0.7f, 0), 0.1f, layerGround);//проверка находимся ли на земле
        anim.SetBool("ground", isGraund);//передаем значение в аниматор на земле или нет персонаж
        anim.SetFloat("ySpeed", rb.velocity.y);//значение оси У из компонента риджид боди, используем параметры просчитывания физики оттуда
        //if (!isGraund)//отключает управление игроком в воздухе
          //  return;
        move = Input.GetAxis("Horizontal");//сохранили значение осей
        anim.SetFloat("xSpeed",Mathf.Abs(move));//значение оси передаем компонент аниматор по ссылке
        rb.velocity = new Vector2(move * speed, rb.velocity.y);//доступ к риджибоди, скорость персонажа (значение по оси х, берем из ридж боди - по оси Y) Mathf.Abs - модуль
        //чтобы персонаж не ходил задом наперд след строки создадим переменную в 10 строке bool
        if (move>0 && !isRight)//если движение больше нуля и перед этим персонаж шел в лево то делаем инверсию(след строка)
        {
            sr.flipX = false;
            isRight = true;
        }
        else if(move < 0 && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }
    }


    void Update()//используем для прыжка
    {
        newPosCam = transform.position + deltaPosCam;
        cam.position = newPosCam;
        par.run = (transform.position.x - posX) * 15f;
        posX = transform.position.x;

        par.jamp = transform.position.y - posY;
        posY = transform.position.y;

        

       if(isGraund && Input.GetKeyDown(KeyCode.Space))//если нажат пробел
        {
            rb.AddForce(new Vector2(0, 300));//сила(по Х и по У)
        }
        if (Input.GetKeyDown(KeyCode.Return))//персонаж стреляет
        {
            // Rigidbody2D newBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            Rigidbody2D newBullet = PoolSys.sys.TakeFromPool(0, transform.position).GetComponent<Rigidbody2D>();
            if (isRight)//в каком направлении 
            {
                newBullet.AddForce(new Vector2(500, 0));
            }
            else
            {
                newBullet.AddForce(new Vector2(-500, 0));
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bonus")//подбирание алмазов
        {
            countDiamond++;
            //gui.SetCountDiamond(countDiamond);
            ui.SetCountDiamond(countDiamond);
            Destroy(collision.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "BulletNPC")
        {
            hp -= 10;
            //gui.SetHP(hp);
            ui.SetHP(hp);
            if (hp <= 0)
            {
                transform.Rotate(0, 0, 90);
               // Camera.main.transform.Rotate(0, 0, -90);
                transform.position -= new Vector3(0, 0.7f, 0);
                //anim.SetTrigger("isDeath");
                sr.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                rb.simulated = false;
                GetComponent<CapsuleCollider2D>();
                this.enabled = false;
            
                
            }
        }
    }
}
