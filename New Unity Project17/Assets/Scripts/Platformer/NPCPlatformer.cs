﻿using System.Collections;
using UnityEngine;

public class NPCPlatformer : MonoBehaviour
{
     public float speed;//скорость движения
     public LayerMask layerGround;//слой кот отвечает за блоки
     public Rigidbody2D bullet;//пуля, префаб
     Animator anim;//dostyp k animatory
     Rigidbody2D rb;//dostyp k RB
     SpriteRenderer sr;
     float move = 1;// нпправление движения 1-вправо, -1 влево
     bool isRight = true;//направление движения тру - вправо, фолс влево
     Transform playerTrans;//ссылка на тарнсформ персонажа
     float timerBullet = 0.5f;//таймер для стрельбы
     bool isShot = true;//когда можно стрелять

     void Start()
     {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim.SetBool("ground", true);
        playerTrans = PlayerControllerPlat.sing.transform;
        StartCoroutine(TimeShot());
        SystemNPC.sing.AddCountNPC();
     }

    //проверка есть ли платформа, если нет, то нпс сдет в другую сторону

    void FixedUpdate()
     {
        if (isRight && !Physics2D.OverlapCircle(transform.position+new Vector3(0.8f,-0.8f,0), 0.1f, layerGround))
        {
            isRight = false;
            sr.flipX = true;
            move = -1;
        }
        else if(!isRight && !Physics2D.OverlapCircle(transform.position + new Vector3(-0.8f, -0.8f, 0), 0.1f, layerGround))
        {
            isRight = true;
            sr.flipX = false;
            move = 1;
        }
        anim.SetFloat("xSpeed", 1);//персонаж всегда в движении поэтому 1
        rb.velocity = new Vector2(move * speed, rb.velocity.y);
     }
    //стрельба нпс с проверкой близости персонажа
    private void Update()
    {
        if(isShot && Vector3.Distance(transform.position, playerTrans.position) < 7)
        {
            if(Mathf.Abs(transform.position.x - playerTrans.position.y) < 1)
            {
                if(isRight && transform.position.x - playerTrans.position.x < 0)
                {
                    //Rigidbody2D newBullet = Instantiate(bullet,transform.position + new Vector3(0.4f * move, 0, 0), Quaternion.identity);

                    Rigidbody2D newBullet = PoolSys.sys.TakeFromPool(1, transform.position + new Vector3(0.4f * move, 0, 0)).GetComponent<Rigidbody2D>();
                    newBullet.AddForce(new Vector2(500, 0));
                }
                else if (!isRight && transform.position.x - playerTrans.position.x > 0)
                {
                    //Rigidbody2D newBullet = Instantiate(bullet, transform.position + new Vector3(0.4f * move, 0, 0), Quaternion.identity);

                    Rigidbody2D newBullet = PoolSys.sys.TakeFromPool(1, transform.position + new Vector3(0.4f * move, 0, 0)).GetComponent<Rigidbody2D>();
                    newBullet.AddForce(new Vector2(-500, 0));
                }
                isShot = false;
                timerBullet = 0.5f;
            }
        }
    }

    //корутина для таймера, обеспечивает промежутки между выстрелами
    IEnumerator TimeShot()
    {
        while (true)
        {
            yield return null;
            timerBullet -= Time.deltaTime;
            if (timerBullet < 0)
            {
                isShot = true;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Bullet")
        {
            SystemNPC.sing.RemoveNPC();
            Destroy(gameObject);
        }

    }
}
