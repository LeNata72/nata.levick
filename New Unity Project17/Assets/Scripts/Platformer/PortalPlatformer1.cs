﻿using UnityEngine;

public class PortalPlatformer1 : MonoBehaviour
{
    public int idLoadScene;
    public Transform crater;
    bool isActiv = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActiv)
        {
            crater.Rotate(0, 0, 30);
            if (Input.GetKeyDown(KeyCode.F))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(idLoadScene);

            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if(collision.GetComponent<PlayerControllerPlat>().countDiamond == 3)
            {
                isActiv = true;
                crater.gameObject.SetActive(true);
            }
        }
    }
}
