﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxPlatformer : MonoBehaviour
{
    public Transform[] pmas;
    public float run;
    public float jamp;
    
    void Update()
    {
        pmas[0].localPosition -= new Vector3(0.1f * Time.deltaTime, 0, 0);
        pmas[1].localPosition -= new Vector3(0.1f * Time.deltaTime, 0, 0);
        for (int i = 0; i < 2; i++)
        {
            if(pmas[i].localPosition.x < -25)
            {
                pmas[i].localPosition += new Vector3(50, 0, 0);
            }
            else if (pmas[i].localPosition.x > 25)
            {
                pmas[i].localPosition -= new Vector3(50, 0, 0);
            }
        }
        if (Mathf.Abs(run) > 0.1)
        {
            pmas[0].localPosition -= new Vector3(0.1f * Time.deltaTime * run, 0, 0);
            pmas[1].localPosition -= new Vector3(0.1f * Time.deltaTime * run, 0, 0);
            pmas[2].localPosition -= new Vector3(0.2f * Time.deltaTime * run, 0, 0);
            pmas[3].localPosition -= new Vector3(0.2f * Time.deltaTime * run, 0, 0);
            pmas[4].localPosition -= new Vector3(0.4f * Time.deltaTime * run, 0, 0);
            pmas[5].localPosition -= new Vector3(0.4f * Time.deltaTime * run, 0, 0);
            for (int i = 2; i < 6; i++)
            {
                if (pmas[i].localPosition.x < -25)
                {
                    pmas[i].localPosition += new Vector3(50, 0, 0);
                }
                else if (pmas[i].localPosition.x > 25)
                {
                    pmas[i].localPosition -= new Vector3(50, 0, 0);
                }
            }


        }

            if(Mathf.Abs(jamp) > 0.1)
            {
                pmas[0].localPosition -= new Vector3(0, Time.deltaTime * jamp, 0);
                pmas[1].localPosition -= new Vector3(0, Time.deltaTime * jamp, 0);
                pmas[2].localPosition -= new Vector3(0,0.8f * Time.deltaTime * jamp, 0);
                pmas[3].localPosition -= new Vector3(0,0.8f * Time.deltaTime * jamp, 0);
                pmas[4].localPosition -= new Vector3(0, 1.6f * Time.deltaTime * jamp, 0);
                pmas[5].localPosition -= new Vector3(0, 1.6f *Time.deltaTime * jamp, 0);

            }
    }
}
