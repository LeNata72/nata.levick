﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomSense : MonoBehaviour
{
    public Transform restartPos;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.transform.position = restartPos.position;
        }
    }
}
