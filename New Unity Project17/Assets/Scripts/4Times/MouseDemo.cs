﻿using UnityEngine;

public class MouseDemo : MonoBehaviour
{
    private void OnMouseEnter()//входит на объект
    {
        print("OnMouseEnter");
    }
    private void OnMouseExit()//выходит
    {
        print("OnMouseExit");
    }
    private void OnMouseOver()//на объекте
    {
        print("OnMouseOver");
    }
    private void OnMouseDown()//нажимаем на об.
    {
        print("OnMouseDown");
    }
    private void OnMouseUp()//отпустили мышь
    {
        print("OnMouseUp");
    }
    private void OnMouseDrag()//находимся на об. с зажатой кнопкой
    {
        print("OnMouseDrag");
    }

    private void OnMouseUpAsButton()//нажали и отпустили кнопку на одном объекте
    {
        print("OnMouseUpAsButton");
    }
}
