﻿using UnityEngine;

public class Baget : MonoBehaviour
{
    private SpriteRenderer sr;
    private bool isRed;
    private float timer;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        SetRandomColorSprite();
        timer = Random.Range(1f, 3f);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if(timer < 0)
        {
            ChangeColor();
            timer = Random.Range(1f, 3f);

        }
    }
    void SetRandomColorSprite()
    {
        int random = Random.Range(0, 2);
        if(random == 0)
        {
            isRed = false;
            sr.color = Color.green;
        }
        else
        {
            isRed = true;
            sr.color = Color.red;
        }
    }
    void ChangeColor()
    {
        // sr.color = isRed ? Color.green : Color.red;
        if (isRed)
            sr.color = Color.green;
        else
            sr.color = Color.red;
        isRed = !isRed;
    }

    private void OnMouseDown()
    {
        if (isRed)
        {
            Freez();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Freez()
    {
        sr.color = new Color(1, 1, 1, 0.4f);
        this.enabled = false;
    }
}
