﻿using UnityEngine;

public class InitGameDR3 : MonoBehaviour
{

    public Baget bagetPref;
    public Transform marker1, marker2;//для обмеження області
    public int countSprites;// кількість спрайтів

    void Start()
    {
        for(int i = 0; i < countSprites; i++)
        {
            Instantiate(bagetPref, new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                Random.Range(marker1.position.y, marker2.position.y), 0), Quaternion.identity, transform);
        }
    }

    
}
