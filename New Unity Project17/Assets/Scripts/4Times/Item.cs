﻿using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField]
    private Sprite[]spritesItem;
    private int id = -1;
    private SpriteRenderer sr;
    private Vector2 marker1, marker2;
    private float timer;
    private float timerBlink = 1.5f;
    private bool isRed = false;
    private bool isFreez = false;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        RandomItem();
        timer = Random.Range(2f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFreez)
        {

            timer -= Time.deltaTime;
            if (timer < 0)
            {
                RandomItem();
                RandomPosition();
                timer = Random.Range(2f, 4f);

            }
            if (id == 0)
            {
                timerBlink -= Time.deltaTime;
                if (timerBlink < 0)
                {
                    timerBlink = 1.5f;
                    isRed = !isRed;
                    if (isRed)
                        sr.color = Color.red;
                    else
                        sr.color = Color.white;
                }
            }
        }
    }
    void RandomItem()
    {
        int randomId;
        do
        {
            randomId = Random.Range(0, spritesItem.Length);
        }while(randomId == id);

        id = randomId;
        sr.sprite = spritesItem[id];
        sr.color = Color.white;
        isRed = false;
        if (id == 0)
        {
            timerBlink = 1.5f;
        }
    }

    void RandomPosition()
    {
        transform.position = new Vector3(Random.Range(marker1.x, marker2.x), Random.Range(marker1.x, marker2.x));
    }

    public void SetMarker(Vector3 marker1, Vector3 marker2)
    {
        this.marker1 = new Vector2(marker1.x, marker2.y);
        this.marker2 = new Vector2(marker1.x, marker2.y);
    }


        private void OnMouseDown()
    {
        if (id == 0)  // це зірка
        {
            if (isRed)   // червона зірка
            {
                InitGame.score -= 3;
                Freez();
            }
            else   // звичайна зірка
            {
                InitGame.score += 3;
                Destroy(gameObject);
            }
        }
        else   // інший об'єкт
        {
            InitGame.score -= 2;
            Destroy(gameObject);
        }
    }

    void Freez()
    {
        isFreez = true;
        sr.color = new Color(1, 0, 0.4f);
        GetComponent<BoxCollider2D>().enabled = false;
    }


}
