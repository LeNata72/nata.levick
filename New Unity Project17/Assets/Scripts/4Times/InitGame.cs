﻿using UnityEngine;

public class InitGame : MonoBehaviour
{
    public static int score = 0;
    public int countStart = 3; // 
    public float timerGame = 30;
    [SerializeField]
    private Item itemPref;
    [SerializeField]
    private Transform marker1, marker2;
    private float timer;
    void Start()
    {
       for(int i = 0; i < countStart; i++)
        {
            AddItem();
        }
        timer = Random.Range(1f, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            AddItem();
            timer = Random.Range(1f, 2f);
        }

        timerGame -= Time.deltaTime;
        if (timerGame < 0)
        {
            print("Game Over\nScore:" + score);
            Destroy(gameObject);
        }
    }
       void AddItem()
        {
            Item temp = Instantiate(itemPref, new Vector3(Random.Range(marker1.position.x,marker2.position.x),
                 Random.Range(marker1.position.y, marker2.position.y), 0),
                   Quaternion.identity, transform);
        temp.SetMarker(marker1.position, marker2.position);
        }
}
