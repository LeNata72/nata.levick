﻿using System.Collections.Generic;
using UnityEngine;

public class NPC
    : MonoBehaviour
{
    public float speed = 1;// скорость движения нпс
    public LayerMask layerBlocks;//объявим лаер стены
    Rigidbody2D rb;//ссылка на компонент
    Vector2 direction;//направление движения

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();//9 строка
        direction = Vector2.up;//будет идти вверх
    }

    void FixedUpdate()// метод преобразовали в фиксед для реализации проверки что находится впереди
    {
        if (direction == Vector2.up && Physics2D.OverlapBox(transform.position + Vector3.up * 0.5f, new Vector2(0.3f, 0.2f), 0, layerBlocks))//OverlapCircle позволяет создать детектор НПС идет вверх
        {
            Turn();//вызов метода поворота нпс
        }
        else if (direction == Vector2.down && Physics2D.OverlapBox(transform.position + Vector3.down * 0.5f, new Vector2(0.3f, 0.2f),0, layerBlocks))
        {
            Turn();
        }
        else if (direction == Vector2.left && Physics2D.OverlapBox(transform.position + Vector3.left * 0.5f,new Vector2(0.2f,0.5f) ,0, layerBlocks))
        {
            Turn();
        }
        else if (direction == Vector2.right && Physics2D.OverlapBox(transform.position + Vector3.right * 0.5f, new Vector2(0.2f, 0.5f), 0, layerBlocks))
        {
            Turn();
        }
        rb.velocity = direction * speed;// указываем движение нпс
    }

    void Turn()//метод поворота нпс
    {
        List<Vector2> dir = new List<Vector2> { Vector2.right, Vector2.left, Vector2.up, Vector2.down };
        dir.Remove(direction);

        for (int i = 0; i < dir.Count; i++)
        {
            if (Physics2D.OverlapCircle(transform.position + (Vector3)dir[i] * 0.5f, 0.2f, layerBlocks))
            {
                dir.RemoveAt(i);
                i--;
            }
        }

        if (dir.Count == 0)//проверка на то что все элементы из списка удалятся
        {
            Debug.LogError("Not direction");
            this.enabled = false;//отключаем скрипт, снимается галочка в инспекторе
        }

        direction = dir[Random.Range(0, dir.Count)];
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
