﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class caterpillar_controller : MonoBehaviour
{
    public float speed = 1; // устанавливаем скорость игрока
    public Rigidbody2D prefabBullet;//ссылка на префаб с указанием физического компонента(тип) чтобы не делать дополнительно геткомпонент
    public float forseBullet = 100;//сила и величина применяемая к bullet
    public Sprite spriteDead;
    Rigidbody2D rb;
    SpriteRenderer sr; //дополнит переменные ссылка на Sprite Renderer
    bool isRight = true;//проверка в каком напривлении двигаемся
    Vector2 direction;//общее направление движения
    int score = 0;
    int hp = 10;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();//инициализация Rigidbody2D
        sr = GetComponent<SpriteRenderer>();//инициализация Sprite Renderer
        direction = Vector2.right;//9 
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            direction = Vector2.right;
            Flip();//вызов метода 
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            direction = Vector2.left;
            Flip();
        }
        else if (Input.GetKeyDown(KeyCode.W))// добавляем направление для пули
        {
            direction = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            direction = Vector2.down;
        }

        if (Input.GetKeyDown(KeyCode.Space))//если нажимаем пробел
        {
            //создаем пулю и придаем ускорение
            Rigidbody2D newBullet = Instantiate(prefabBullet, transform.position, Quaternion.identity);//создаем на позиции нашего персонажа Quaternion - поворот
            newBullet.AddForce(direction * forseBullet); // обращаемся к пуле ссылка на риджидбоди гед будет addaforse
            //- разрешает приложить силы - привязка к direction (направление) 
        }
    }
    private void FixedUpdate() //для вычисления физики
    {
        rb.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))* speed;//скорость движения гусеницы по направлениям
    }

    void Flip()//метод для  Input.GetKeyDown 20 и 25 строка
    {
        if (direction == Vector2.left && isRight) //если персонаж смотрит влево а двигается вправо
        {
            sr.flipX = true;//исполняем флип на спрайте, если спрайт двигается влево то и будет смотреть влево
            isRight = false;
        }
        else if (direction == Vector2.right && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)//проверка тега объекта в кот попадаем
    {
        if (collision.tag == "Bonus")
        {
            if (collision.name.Contains("Golden-lolypop"))
            {
                score += 10;
            }
            else if (collision.name.Contains("Rise-ball"))
            {
                score += 5;
            }
            else if (collision.name.Contains("War-fan"))
            {
                score += 3;
            }
            Destroy(collision.gameObject);//удаляем бонус
            print("Score: " + score);
        }
            
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            hp -= 5;
            if (hp <= 0)
            {
                sr.sprite = spriteDead;
                rb.simulated = false;
                GetComponent<CapsuleCollider2D>().enabled = false;
                this.enabled = false;
                Time.timeScale = 0;

            }
        }
    }
}
