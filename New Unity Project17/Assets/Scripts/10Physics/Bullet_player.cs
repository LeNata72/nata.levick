﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_player : MonoBehaviour
{
    PoolObj poolObj;
    private void Start()
    {
        poolObj = GetComponent<PoolObj>();
    }
    //если isTriger включен то срабатывает OnCollision 
    // через collision получаем информацию про объект на кот попал bullet
    private void OnCollisionEnter2D(Collision2D collision)
    {

        // Destroy(gameObject);//уничтожаем bullet
        poolObj.DestroyObj();
    }

    private void OnBecameInvisible()
    {
        //Destroy(gameObject);//уничтожается за пределами видимости
        poolObj.DestroyObj();
    }
}
