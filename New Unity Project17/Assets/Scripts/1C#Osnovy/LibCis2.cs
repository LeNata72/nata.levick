﻿using UnityEngine;
using System;

class Man
{
    public Sex sex;
    private float _masa;
    private string _name;
    private int _yearOfBirth;

    public int Age
    {
        get
        {
            return DateTime.Today.Year - _yearOfBirth;
        }
    }

    public int YearOfBirth
    {
        get
        {
            return _yearOfBirth;
        }
        set
        {
            if (value >= 1900)
            {
                _yearOfBirth = value;
            }
            else
            {
                
                Debug.LogError("Неверный год рождения");
            }
        }
    }

    public float Masa
    {
        get
        {
            return _masa;
        }
        set
        {
            if (value > 0)
            {
                _masa = value;
            }
            else
            {
                Debug.LogError("Некорректный вес");
            }
        }
    }


    //конструктор

    public Man()
    {
        _name = "noname";
        _yearOfBirth = 1900;
        _masa = 1f;
        sex = Sex.n_a;
    }

    public Man(string name, Sex sex, int yearOfBirth, float masa)
    {
        _name = name;
        this.sex = sex;
        YearOfBirth = yearOfBirth;
        Masa = masa;
    }

    public string StrInfo()
    {
        return "Name:" + _name + "; sex:" + sex + "; yearOfBirth:" + YearOfBirth + "; Masa:" + Masa;
    }

    public void printInfo()
    {
        Debug.Log("Name:" + _name + "; sex:" + sex + "; yearOfBirth:" + YearOfBirth + "; Masa:" + Masa);
          
    }

    public enum Sex
    {
        Man,
        Woman,
        n_a
    }

}

class Student : Man
{
    public int yearOfStudy;



    public Student(string name, Sex sex, int yearOfBirth, float masa, int yearOfStudy) : base(name, sex, yearOfBirth, masa)
    {
        this.yearOfStudy = yearOfStudy;
    }

    public void PrintInfo()
    {
        base.printInfo();
        Debug.LogFormat("Год обучения:{0}", yearOfStudy);
    }
    //void - тип метода що не повертає результат
    public void SetYearOfStudy(int newYearOfStudy)
    {
        yearOfStudy = newYearOfStudy;
    }

    public void AddYearOfStudy()// параметр вказувати не потрібно
    {
        yearOfStudy++; 
    }
}

//___________________________________3

class Euro: Money
{
    public int cinaEuro;


    public Euro(int cinaEuro, int first, int second): base(first,second)
    {
        this.cinaEuro = cinaEuro;
    }

    public void PrintInfo()
    {
        Display();
        Debug.LogFormat("Цінa євро:{0}", cinaEuro);
    }

   
}


