﻿using UnityEngine;

public class NaDomy : MonoBehaviour
{
    void Start()
    {
        //ЛПР 2 _____________________________________№1
        // масу в кг записати в тоннах
        print("№-1  масу в кг записати в тоннах");
        int kg = 3233;
        int tn = 1000;

        float tonny = kg/(float)tn;

        Debug.LogFormat("кг: {0}    тонны: {1}", kg, tonny);

        //лпр 2_________________________________________2a
        //значення виразiв де замiсть невiдомих рандомнi значення
        print("№-2  значення виразiв де замiсть невiдомих рандомнi значення");

        int a = Random.Range(1, 121);//1..120
        int b = Random.Range(1, 121);
        int x = Random.Range(1, 121);
        int c = Random.Range(1, 121);

        float y =1/ Mathf.Sqrt(a * (x * x) + b * x + c);

        Debug.LogFormat("a ={0} b ={1} c ={2} x ={3} y ={4} ", a, b, c, x, y);
        //лпр2____________________________________________2b

        float result = Mathf.Sqrt(x+1) + Mathf.Sqrt(x-1) / (2 * Mathf.Sqrt(x));

        print(result);

        //лпр2_________________________________________2c

        float rez = Mathf.Sqrt(a * a + b * b - 2 * a * b * Mathf.Cos(c));

        print(rez);
        //lpr2________________________________________3
        // знайти сер арифм та геометричне 2 чисел(константи)
 
        int d = 25;
        int i = 35;

        float ser = (d + i) / 2;
        float geom = Mathf.Sqrt(d + i);

        Debug.LogFormat(" (№ 3 ЛПР-2 знайти сер арифм та геометричне 2 чисел) Ser arifmet: {0} Ser geom:{1}", ser, geom);

        //________________________________________4
        // розрахунок значень функцiй де а та b рандомнi
        print("№-4  розрахунок значень функцiй де а та b рандомнi");
        float rezx = ((2 / (a * a + 25)) + b) /(Mathf.Sqrt(b) + (a + b) / 2);
        print (rezx);

        //________________________________________5
        //значення температури по Цельсiю перетворити на значення по Фаренгейту та Кельвiну

        //a = t C
        float fahrenheit = (a * 1.8f) + 32;
        float kelvin = a + 273.15f;

        Debug.LogFormat("(№ 5 ЛПР-2) t C: {0} t F: {1} t K {2}", a, fahrenheit, kelvin);

        //_______________________________________6______________
        // a вступiнi 6 за 3 операцii, 8 за 3 оп, 15за 5 операцiй, 64 - 6

        float a2 = a * a;     //a vo vtoroy ct   для 64 1 операция
        float a4 = a2 * a2;   // a v 4 ct         2-я
        float a6 = a4 * a2;   // a v 6 ct 

        float a8 = a4 * a4;   // a v 8 ct          3-я

        float a14 = a8 * a6;    // a v 14 ct
        float a15 = a14 * a;   //a v 15 ct

        float a16 = a8 * a8;      //a v 16 ct         4-я
        float a32 = a16 * a16;    //a v  32 ct        5 - я
        float a64 = a32 * a32;    // a v 64 ct        6 -я

        Debug.LogFormat("(№ 6) значення а: {0}  a в 6-й ступ: {1} a в 8-й ступ: {2} a в 15-й ступ: {3} a в 64-й ступ {4}", a, a6, a8, a15, a64);

        //_____________________________________________7______________________
        int sec = Random.Range(100, 500001); //повна кiлькiсть секунд з початку доби

        int hod = sec / (60*60); // годин з початку черговоi доби
        int osthv = (sec % 3600)/60;  // хвилин з початку черговоi години
        int ostcek = sec - (sec / 60 * 60);

        Debug.LogFormat("(№ 7)З початку доби пройшло: повна кiлькiсть секунд {0} з них: {1} годин:   {2} хвилин:  {3} сек", sec, hod, osthv, ostcek);
        //___________________________________________________8________________

        int nomer = Random.Range(101,1000); 
        int edin = nomer % 10;
        int dvi = nomer / 10;

        Debug.LogFormat("(№8) число: {0} результат: {1}{2}", nomer, edin, dvi);

        //_________________________________________________9___________________

        int chis = Random.Range(1000, 10000);

        int summa = chis / 1000 + (chis / 100) % 10 + (chis / 10) % 10  + chis % 10;

        Debug.LogFormat("(№9) число: {0} сума чисел: {1}", chis, summa);


        //_____________________________лпр3
        //________________________________________1_____________________________
        //int b, x, c
        Debug.LogFormat("ЛПР 3-4 № - 1 число b: {0} число x: {1} число c: {2} ", b, x, c);
        if (b > x && b>c)
        {
            Debug.LogWarning(" b - найбiльше число");
        }
        else if ( x > b && x > c)
        {
            Debug.LogWarning("x - найбiльше число ");
        }
        else if (c > b && c > x)
        {
            Debug.LogWarning(" c - найбiльше число");
        }
        else
        {
            Debug.LogWarning(" нема жодного найбiльшого числа");
        }


        // ________________________________________3____________________________

        // а = швидкiсть в км за год
        // b = швидкiсть в метрах за секунду

        float ms = a / 3.6f;
        Debug.LogFormat("Speed кm/h: {0}    m/sek: {1}", a, b);

        if (b > ms)
        {
            Debug.LogWarning(" speed a > speed b");
        }
        else if (b > ms)
        {
            print("speed a < speed b");
        }
        else
        {
            print("speed a=speed b");
        }

        //_________________________________4

        int number = Random.Range(100, 1000);
        print(number);
        int num1 = number / 100;
        int num2 = (number % 100) / 10;
        int num3 = number % 10;

        if (num1 == num2 && num2 == num3)
        {
            print("все цифры одинаковые");
        }
        else if(num1 == num2 || num2 == num3 || num3 == num1)
        {
            print("минимум 2 цифры одинаковые");
        }
        else
        {
            print("нет одинаковых цифр");
        }


        //_______________________________________5_____________________
        //конверт
        int ak = Random.Range(200, 401);// 200;
        int bk = Random.Range(200, 401);// 300;
        //листiвка
        int cl = Random.Range(199, 400);//199;
        int dl = Random.Range(199, 400);//299; // можна використати закоментованi числа


        Debug.LogFormat("ЛПР 3 № - 5 Е конверт зi сторонами a: {0} та b: {1} листiвка зi сторонами  c: {2} та d: {3} ", ak,  bk, cl, dl);


        if ((ak - 1) == cl && (bk - 1) == dl)
        {
            print("Листiвка помiститься в конверт");
        }
        else if(ak <= cl && bk <= dl)
        {
            print(" Не помiститься! Обрiжте листiвку по краям, або згорнiть");
        }
        else
        {
            print("Листiвка мала, помiститься в конверт, але використання такого великого конверта для такоi маленькоi листiвки недоцiльне");
        }

        //_________________________________________________6_____________________
        // a
        print("ЛПР 3-4 №-6 a");
        int z = Random.Range(12, 100);//12..99
        print(z);

            if (z % 10 == 3 || z / 10 == 3)
            {
                print("У складi числа е цифра 3");
            }
            else
            {
                print("У складi числа нема цифри 3");
            }

        //   b
        print("ЛПР 3-4 №-6 b");
        int m = Random.Range(1,10);//1..9
        print("m = " + m);

            if (z % 10 == m || z / 10 == m)
            {
                print("У складi числа е цифра m");
            }
            else
            {
                print("У складi числа нема цифри m");
            }

        //_____________________________________________________7_______________________
        print("ЛПР 3-4 №-7");


        int R = Random.Range(1000, 3000);
        float vis = R % 400;
        float vis1 = R % 4;

        if(R == 2000 || R == vis || R == vis1)
        {
            print(R + "рiк високосний");
        }
        else
        {
            print(R + " рiк не високосний");
        }

        //_________________________________________________________8_______________________
        print("ЛПР 3-4 №-8");
       int pr = Random.Range(1, 13);
       print("мiсяць = " + pr);
       switch (pr)
       {
           case 12:
           case 1:
           case 2:
               print("Зима");
               break;
           case 3:
           case 4:
           case 5:
               print("Весна");
               break;
           case 6:
           case 7:
           case 8:
               print("Лiто");
               break;
           case 9:
           case 10:
           case 11:
               print("Осiнь");
               break;
           default:
               print("Такого мiсяця нема");
               break;
       }
        //_________________________________________________________9_____________________________
        //Дано натуральне число, що визначає вік людини в місяцях. Знайти вiк в роках i мiсяцях. Оператор вибору
        print("ЛПР 3-4 №-9");
        // ..1 - рiк
        // ..2, 3, 4 - роки
        // iнше - рокiв
        int mic = Random.Range(12, 1201);
        int rik = mic / 12;//скiльки рокiв
        int mis = mic % 12;//скiльки залишаеться мiсяцiв
        print(mic + "всього мiсяцiв");
        int rikZ;//додаткова змiнна
        if (rik >= 5 && rik <= 20)
        {
            rikZ = rik;
        }
        else
        {
            rikZ = rik % 10;
        }
        string str = "";

        switch (rikZ)
        {
            case 1:
                str = rik + "рiк";
                break;
            case 2:
            case 3:
            case 4:
                str = rik + "роки";
                break;
            default:
                str = rik + "рокiв";
                break;
        }
        str += " ";
        switch (mis)
        {
            case 1:
                str += mis + "мiсяць";
                break;
            case 2:
            case 3:
            case 4:
                str += mis + "мiсяцi";
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                str += mis + "мiсяцiв";
                break;
        }

        print(str);


    }
}
