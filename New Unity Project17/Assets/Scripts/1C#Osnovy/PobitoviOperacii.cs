﻿using UnityEngine;

public class PobitoviOperacii : MonoBehaviour
{
   
    void Start()
    {
 //___________________________________10_________________________
        //Використовуючи побітові операції виконати піднесення до степеня 2n
        // де n - ціле додатне число.
        print("                                   ЛПР 3-4");
        print("   № 10");
  
        int n = Random.Range(2, 11);
        int x = 1 << n;
        print(x);

        //__________________________________________11_______________________
        /*int i = 333;
        string str = "";

        while (i > 1)
        {
            str = (i % 2) + str;
            i = i / 2;
        }
        str = "1" + str;
        print(str);
        //___________________________________________12______________________
        int m = 9;
        int s = 2;
        int b = 1 << s;
        m = m | b;
        print(m);
        */

    }

}
