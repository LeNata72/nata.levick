﻿using System.Collections.Generic;
using UnityEngine;

public class OOP3 : MonoBehaviour
{
    void Start()
    {
        /*List<Komputer> parametry = new List<Komputer>();
        parametry.Add(new Vinchester(768000, "intel i11", 8.429f, 16));
        parametry.Add(new Yakist());
        print(parametry.Count);

        foreach (var p in parametry)
        {
            p.PrintInfo();
        }*/

	Car car = new Car("Nissan", 140);

        List<Car> cars = new List<Car>();
        cars.Add(new Car("Nissan", 140));
        cars.Add(new ExecutiveCar("Nissan X",160));

        foreach (var p in cars)
        {
            p.PrintInfo();
        }

    }


}