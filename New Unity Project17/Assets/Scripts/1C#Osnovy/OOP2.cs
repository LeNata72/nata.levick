﻿using UnityEngine;

public class OOP2 : MonoBehaviour
{
   
    void Start()
    {
        /*Student man = new Student("Patrik", Man.Sex.Man, 1990, 65f,2019);
        man.PrintInfo();
        //print(man.yearOfStudy);
        man.AddYearOfStudy();
        man.PrintInfo();
        */
        Money[] bank = new Money[3];
        bank[0] = new Money(5, 20);
        bank[1] = new Money(500, 10);
        bank[2] = new Money(200, 5);

        int suma = 0;
        foreach (Money m in bank)
        {
            m.Display();
            suma += m.Suma();
        }
        print("Suma: " + suma);

    }

    
}




