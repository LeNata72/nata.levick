﻿using UnityEngine;
using System; //системний час для вирахування віку

class Test
{
    //поля
    public string name;
    public int id;
    private int _count;

    

    //властивості
    public int Count
    {
        //повертає значення закритого поля
        get
        {
            return _count;
        }
        //встановлює значення закритого поля повний варіант
        set
        {
            _count = value;
        }
    }

    public float MaxSpeed { get; } //скорочений варіант автоматично реалізує поле

    //конструктори
    public Test()//перший без параметрів назва класу має бути
    {
        name = "noname";
        id = 0;
        _count = 0;
        MaxSpeed = 1;
        Debug.Log("Hello1");
    }

    public Test(string name, int id, int count, float maxSpeed)
    {
        this.name = name;
        this.id = id;
        Count = count;
        MaxSpeed = maxSpeed;
        Debug.Log("Hello2");


    }

    //деструктор коли обект знищується (сміття)необязат
    ~ Test()
    {
        Debug.Log("pa-pa");
    }




    //методи
    public void PrintInfo()
    {
        Debug.LogFormat("Name: {0}; id:{1}; count:{2}", name, id, _count);
    }


}

//-------------------------------------------------
class Human
{
    public Sex sex;
    //public string sex2;
    public string colorOfEyes;
    private float _height; // metr
    private float _masa; // kilogram
    private string _name;
    private int _yearOfBirth;

    public int Age
    {
        get
        {
            return DateTime.Today.Year - _yearOfBirth;
        }
    }

    public int YearOfBirth
    {
        get
        {
            return _yearOfBirth;
        }
        set
        {
            //_yearOfBirth = value >= 1900 ? value : 1900; тернарний
            if (value >= 1900)
            {
                _yearOfBirth = value;
            }
            else
            {
                _yearOfBirth = 1900;
                Debug.LogError("Некоректний рік народження");
            }
        }
    }

    public float Height
    {
        get
        {
            return _height;
        }
        set
        {
            if (value > 0)
            {
                _height = value;
            }
            else
            {
                Debug.LogError("некоректний зріст");
            }
        }
    }

    public float Masa
    {
        get
        {
            return _masa;
        }
        set
        {
            if (value > 0)
            {
                _masa = value;
            }
            else
            {
                Debug.LogError("некоректна вага");
            }
        }
    }



    // конструктруктор

    public Human() // конструктор без параметрів
    {
        _name = "noname";
        _yearOfBirth = 1900;
        _masa = 1f;
        _height = 1;
        sex = Sex.n_a;
        colorOfEyes = "black";

    }



    public Human(string name, Sex sex, int _yearOfBirth, float masa, float height, string colorOfEyes)
    {
        _name = name;
        this.sex = sex;
        YearOfBirth = _yearOfBirth;
        Masa = masa;
        Height = height;
        this.colorOfEyes = colorOfEyes;
    }

    public string StrInfo()
    {
        return "Name: " + _name + ";  sex:" + sex + ";  " + YearOfBirth + ";  Age" + Age +
            ";  Masa:  " + Masa + ";  Height:  " + Height + ";  colorOfEyes: " + colorOfEyes;
    }


}

enum Sex
{
    Man,
    Woman,
    n_a
}
//--------------------------------------------------------
class Money
{
    private int _first;
    private int _second;

    public int First
    {
        get
        {
            return _first;
        }
        set
        {
            if (value > 0)
            {
                switch (value)
                {
                    case 1:
                    case 2:
                    case 5:
                    case 10:
                    case 20:
                    case 50:
                    case 100:
                    case 200:
                    case 500:
                        {
                            _first = value;
                            break;
                        }
                    default:
                        {
                            _first = 1;
                            Debug.LogError("Nevirhyy nominal");
                            break;
                        }
                }
            }
            else
            {
                _first = 1;
                Debug.LogError("Nevirhyy nominal");
            }
        }
    }

    public int Second
    {
        get
        {
           return _second;
        }
        set
        {
            if(value>0)
            {
                _second = value;
            }
            else
            {
                Debug.LogError("Kilkist mae buty > 0");
            }
        }
    }

    //konstruktor
    public Money()
    {
        _first = 1;
        _second = 1; //параметрам присвоюєм коректне значення
    }

    public Money(int first, int second)
    {
        First = first;
        Second = second;//присвоюєм значення
    }

    public void Display()
    {
        Debug.LogFormat("Nominal: {0};  Kilkist:{1}", _first, _second);
    }

    public int Suma()
    {
        return _first * _second;
    }
}
//---------------------2b

class Vremya
{
    private int _first;
    private int _second;

    public int First
    {
        get
        {
            return _first;
        }
        set
        {
            if (value > 0)
            {
                _first = value;
            }
            else
            {
                Debug.LogError("Kilkist mae buty > 0");
            }
        }
            
    }

    public int Second
    {
        get
        {
            return _second;
        }
        set
        {
            if (value > 0)
            {
                _second = value;
            }
        }

    }
    public Vremya()
    {
        _first = 1;
        _second = 1; //параметрам присвоюєм коректне значення
    }

    public Vremya(int first, int second)
    {
        First = first;
        Second = second;//присвоюєм значення
    }

    public void Display()
    {
        Debug.LogFormat("Minuty: {0};  Sekundy:{1}", _first, _second);
    }

    public int Suma()
    {
        return _first * 60 + _second;
    }
}