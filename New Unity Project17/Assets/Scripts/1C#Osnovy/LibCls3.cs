using UnityEngine;

class Komputer
{
    public string name;
    public float chastota;
    public int operativka;

    public Komputer (string name, float chastota, int operativka)
    {
        this.name = name;
        this.chastota = chastota;
        this.operativka = operativka;
    }

    public virtual void PrintInfo()
    {
        Debug.LogFormat("��������: {0}; �������: {1}; ���������� ���'���: {3}", name, chastota, operativka);
    }

    public virtual float Yakist()
    {
        return (0.1f * chastota) + operativka;
    }


}

    class Vinchester: Komputer
    {
        public int obyem;

        public Vinchester (int obyem, string name, float chastota, int operativka): base(name,chastota,operativka)
        {
            this.obyem = obyem;
        }

        public override void PrintInfo ()
        {
            Debug.LogFormat("��'�� ���������: ", obyem);
        }

        public override float Yakist()
        {
            return (base.Yakist() + 0.5f * obyem);
        }
    }


class Car
{
    public string name;
    public int maxSpeed;
    //public int cina;

    public Car(string name, int maxSpeed)
    {
        this.name = name;
        this.maxSpeed = maxSpeed;
       
    }


    public virtual void PrintInfo()

    {
        Debug.LogFormat("�����: {0};  ����������� �����i���: {1} km/h; �i��: {2};", name, maxSpeed, Cina());
    }


    public virtual int Cina()
    {
        return maxSpeed * 100;
    }

    public virtual void newSpeed()
    {
        maxSpeed += 10;
    }
}


class ExecutiveCar: Car
    {
  

    public ExecutiveCar(string name, int maxSpeed):base(name, maxSpeed)
    {
         
    }


    public override int Cina()
    {
        return maxSpeed * 250;
    }

    public override void newSpeed()
    {
         maxSpeed += 5; // ��� ����� �� ����������, ����������� ����
    }

    public override void PrintInfo()
    {
        Debug.LogFormat("�i��: {0}; ����������� �����i���: {1} km/h;", Cina(), maxSpeed);

    }
}



