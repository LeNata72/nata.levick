﻿using UnityEngine;

public class BaseObjN2 : MonoBehaviour
{
    void Start()
    {
        for (int i = 0; i < 8; i++)
        {
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            obj.transform.position = new Vector3(i + i, 0, 0);
            obj.name = "Sphere" + i;
        }
    }

}
