﻿using System.Globalization;
using UnityEngine;

public class BaseObjNum1 : MonoBehaviour
{
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            obj.transform.position = new Vector3(i+i, 0, 0);
            obj.name = "CUBE" + i;
        }
    }

}
