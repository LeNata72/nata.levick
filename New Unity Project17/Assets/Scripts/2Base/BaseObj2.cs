﻿using UnityEngine;

public class BaseObj2 : MonoBehaviour
{
    void Start()
    {
        GameObject.Find("Cylinder").transform.position += new Vector3(0, 2, 0);
        GameObject[] goArray = GameObject.FindGameObjectsWithTag("Bonus");
        for(int i = 0; i <goArray.Length; i++)
        {
            goArray[i].GetComponent<MeshRenderer>().material.color =
                new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }

}
