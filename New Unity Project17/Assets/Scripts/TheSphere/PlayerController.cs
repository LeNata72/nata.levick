﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody playerRigibody;
    void Start()
    {
        playerRigibody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float forvardMove = Input.GetAxis("Vertical");
        float sideMove = Input.GetAxis("Horizontal");

        playerRigibody.AddForce(Vector3.forward * forvardMove);
        playerRigibody.AddForce(Vector3.right * sideMove);
    }
}
