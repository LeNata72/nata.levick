﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController3D : MonoBehaviour
{
    public float speed = 3;
    public float mouseSensety = 2;
    public bool isMouseInversion = false;
    public Rigidbody bullet;
    public Transform gun;
    public Text textAmmo;
    public static PlayerController3D player;
    int countAmmo = 0;
    public Text textHP;
    int countHP = 0;

    Rigidbody rb;
    Transform cam;
    Vector3 rotCam;

    bool isDamage = false;

    public RectTransform panelTask;
    float panelTaskY;

    void Awake()
    {
        player = this;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        AddAmmo(10);
        AddHP(200, Vector3.zero);
        panelTaskY = panelTask.position.y;
    }

    private void FixedUpdate()
    {
        if (!isDamage)
        {
            rb.velocity = transform.TransformDirection(Input.GetAxis("Horizontal") * speed, rb.velocity.y, Input.GetAxis("Vertical") * speed);
        }
        transform.Rotate(0, Input.GetAxis("Mouse X") * mouseSensety, 0, Space.World);
        rotCam = cam.localEulerAngles;
        cam.Rotate(Input.GetAxis("Mouse Y") * mouseSensety * (isMouseInversion ? 1 : -1), 0, 0, Space.Self);
        if ((cam.localEulerAngles.x > 310 && cam.localEulerAngles.x < 360) || (cam.localEulerAngles.x > 0 && cam.localEulerAngles.x < 30))
        { }
        else
        {
            cam.localEulerAngles = rotCam;
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Physics.Raycast(transform.position, Vector3.down, 1.2f))
        {
            rb.AddForce(0, 300, 0);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            if (panelTask.position.y == panelTaskY)
            {
                panelTask.position += Vector3.up * 2000;//0,1,0 * 2000
            }
            else
            {
                panelTask.position += Vector3.down * 2000;
            }
        }
        if (Input.GetMouseButtonDown(0) && countAmmo > 0)
        {
            Rigidbody temp = Instantiate(bullet, gun.position, Quaternion.identity);
            temp.AddForce(gun.TransformDirection(0, 0, 800));
            AddAmmo(-1);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "BulletNPC")
                {
                    Destroy(hit.transform.gameObject);
                }
                else
                {
                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    obj.GetComponent<MeshRenderer>().material.color = Color.red;
                    obj.transform.parent = hit.collider.transform;
                    Destroy(obj.GetComponent<SphereCollider>());
                    Destroy(obj, 1);
                }

                if (hit.transform.tag == "NPC")
                {
                    hit.transform.GetComponent<NavNPC>().DamageNPC();
                }

            }
        }


    }

    void AddAmmo(int count)
    {
        countAmmo += count;
        textAmmo.text = countAmmo.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bullet")
        {
            AddAmmo(1);
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "BulletNPC")
        {
            AddHP(-10, Vector3.zero);
        }

        else if (collision.transform.tag == "Bonus")
        {
            panelTask.GetComponent<SysTask>().AddItemTask(0);
            Destroy(collision.gameObject);
        }
    }



    public void AddHP(int count, Vector3 posEnemy)
    {
        countHP += count;
        textHP.text = countHP.ToString();
        if (countHP <= 0)
        {
            Time.timeScale = 0;
            print("GAME OVER!!!");
        }

        if (posEnemy != Vector3.zero)
        {
            rb.AddForce((transform.position - posEnemy) * 300);
            isDamage = true;
            StartCoroutine(TimerDamage());
        }
    }

    IEnumerator TimerDamage()
    {
        yield return new WaitForSeconds(0.5f);
        isDamage = false;
    }
}
