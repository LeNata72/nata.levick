﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCControl : MonoBehaviour
{
    public NavNPC npc;
    public Transform[] pointPatrul;

    void Start()
    {
        StartCoroutine(ScanNPC());
    }
    IEnumerator ScanNPC()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (transform.childCount < 3)
            {
                NavNPC temp = Instantiate(npc, transform.position, Quaternion.identity, transform);
                temp.area = pointPatrul;
                temp.Init();
            }
        }
    }
}
