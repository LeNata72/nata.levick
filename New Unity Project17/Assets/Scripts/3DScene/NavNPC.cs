﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NavNPC : MonoBehaviour
{
    public Transform[] area;
    NavMeshAgent agent;
    Animator anim;
    Vector3 pointPatrul;
    Transform player;
    bool isAttackWalk = false;
    bool isAttack = false;
    int hp = 30;

    void Start()
    {
        Init();
    }

    void Update()
    {
        anim.SetFloat("speed", agent.velocity.magnitude);
        float distanc = Vector3.Distance(transform.position, player.position);
        if (distanc < 15)
        {
            if (!isAttackWalk)
            {
                Debug.DrawRay(transform.position, player.position - transform.position, Color.red);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, player.position - transform.position, out hit))
                {
                    if (hit.transform.tag == "Player")
                    {
                        isAttackWalk = true;
                    }
                }
            }
            else
            {
                agent.SetDestination(player.position);
                if (distanc < 2 && !isAttack)
                {
                    isAttack = true;
                    anim.SetTrigger("attack");
                }
            }
        }
        else if (isAttackWalk)
        {
            isAttackWalk = false;
            agent.SetDestination(player.position);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bullet")
        {
            DamageNPC();
        }
    }

    void NextPointPatrul()
    {
        pointPatrul = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            pointPatrul[i] = Random.Range(area[0].position[i], area[1].position[i]);
        }
        agent.SetDestination(pointPatrul);
        StartCoroutine(Stoped());
    }

    IEnumerator Stoped()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            yield return null;
            if (agent.velocity.magnitude < 0.1)
            {
                agent.SetDestination(transform.position);
                yield return new WaitForSeconds(3);
                NextPointPatrul();
                break;
            }
        }
    }

    public void Damage()
    {
        PlayerController3D.player.AddHP(-20, transform.position);
    }

    public void EndAttack()
    {
        isAttack = false;
    }

    public void DamageNPC()
    {
        hp -= 10;
        if (hp<=0)
        {
            PlayerController3D.player.panelTask.GetComponent<SysTask>().AddItemTask(1);
            StopAllCoroutines();
            Destroy(gameObject);
        }
    }

    public void Init()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        NextPointPatrul();
        player = PlayerController3D.player.transform;
    }
}
