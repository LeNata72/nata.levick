﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SysTask : MonoBehaviour
{
    public GameObject panelWinner;
    public MyTask[] arrayTask;
    private GameTask[] panelsTast;
    void Start()
    {
        panelsTast = GetComponentsInChildren<GameTask>();
        if (arrayTask.Length == panelsTast.Length)
        {
            for (int i = 0; i < arrayTask.Length; i++)
            {
                panelsTast[i].Init(arrayTask[i].task, arrayTask[i].countTarget);
            }
        }

    }

    public void AddItemTask(int id)
    {
        panelsTast[id].AddCount();

        bool isWinner = true;
        foreach (var item in panelsTast)
        {
            if (!item.imageFlag.enabled)
            {
                isWinner = false;
                break;
            }
        }
        if (isWinner)
        {
            panelWinner.SetActive(true);
            Time.timeScale = 0;
        }
    }
}

[System.Serializable]
public class MyTask
{
    public string task;
    public int countTarget;
}
