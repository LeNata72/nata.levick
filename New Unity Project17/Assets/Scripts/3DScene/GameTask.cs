﻿using UnityEngine;
using UnityEngine.UI;


public class GameTask : MonoBehaviour
{
    public Text textTask;
    public Text textCountTarget;
    public Image imageFlag;
    private int countTarget;
    private int countCarrent;
    public void Init(string newTast, int newTargetTask)
    {
        textTask.text = newTast;
        countTarget = newTargetTask;
        textCountTarget.text = $"{ countCarrent}/{countTarget}";// 0/5 -> 1/5 5/5(flag)
        imageFlag.enabled = false;
    }

    public void AddCount()
    {
        countCarrent++;
        if (countCarrent == countTarget)
        {
            textCountTarget.enabled = false;
            imageFlag.enabled = true;
        }
        else
        {
            textCountTarget.text = $"{ countCarrent}/{countTarget}";

        }
    }


}
