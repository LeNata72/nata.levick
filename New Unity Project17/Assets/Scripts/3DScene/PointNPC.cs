﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointNPC : MonoBehaviour
{
    public float radiusAttack = 10f;
    public Rigidbody bullet;
    public Transform shotPoint;
    bool isShot = true; // ознака того, що можна стріляти
    Transform player;

    private void Start()
    {
        player = PlayerController3D.player.transform;
    }

    void Update()
    {
        if (isShot)
        {
            if (Vector3.Distance(transform.position, player.position) < radiusAttack)
            {
                shotPoint.LookAt(player);
                ShotTower(player);
            }
            else
            {
                shotPoint.localEulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
                ShotTower(null);
            }
        }
    }

    void ShotTower(Transform target)
    {
        Rigidbody temp = Instantiate(bullet, shotPoint.position, Quaternion.identity);
        temp.AddForce(shotPoint.TransformDirection(0, 0, 1000));
        isShot = false;
        temp.GetComponent<BulletTower>().target = target;
        StartCoroutine(TimerShot());
    }

    IEnumerator TimerShot()
    {
        yield return new WaitForSeconds(2);
        isShot = true;
    }
}
