﻿using UnityEngine;

public class CameraMapMuve : MonoBehaviour
{
    public float distans = 50;
    Transform player;
    void Start()
    {
        player = PlayerController3D.player.transform;
    }

    
    void Update()
    {
        transform.position = player.position + Vector3.up * distans;
    }
}
