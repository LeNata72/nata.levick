﻿using UnityEngine;

public class BulletTower : MonoBehaviour
{
    public float speed = 5f;
    public Transform target;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        if (target)
        {
            transform.LookAt(target);
            rb.velocity = transform.TransformDirection(0, 0, speed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            Destroy(gameObject);
        }
        else
        {
            transform.localScale = new Vector3(3, 3, 3);
            Destroy(gameObject, 3);
        }
        if (collision.transform.tag == "Bullet")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
