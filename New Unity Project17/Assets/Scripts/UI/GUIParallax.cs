﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIParallax : MonoBehaviour
{
    public Texture textureHP;
    public Texture textureActiveDiamond;
    public Texture textureNotActiveDiamond;

    


    float maxHP = 20;
    int hp = 20;
    int countDiamond = 0;

    public void InitGUI(int hp)
    {
        maxHP = hp;
        this.hp = hp;
    }

    public void SetHP(int hp)
    {
        this.hp = hp;
    }

    public void SetCountDiamond(int countDiamond)
    {
        this.countDiamond = countDiamond;
    }
    private void OnGUI()
    {
        GUI.BeginGroup(new Rect(10, 10, 100, 40));
        GUI.Box(new Rect(10, 10, 100, 40), "");
        for (int i = 0; i <= 3; i++)
        {
            if (countDiamond >= i)
            {
                 GUI.DrawTexture(new Rect(10 * i + (20 * (i - 1)), 10, 20, 20), textureActiveDiamond);
            }
            else
            {
                GUI.DrawTexture(new Rect(10 * i + (20 * (i - 1)), 10, 20, 20), textureNotActiveDiamond);
            }
        }
        GUI.EndGroup();

        GUI.DrawTexture(new Rect(0, Screen.height - 30, hp/maxHP * Screen.width, 20), textureHP);
    }


}
