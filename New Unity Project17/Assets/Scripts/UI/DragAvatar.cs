﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DragAvatar : MonoBehaviour
{
    Image img;
    Image imgMouse;
    void Start()
    {
        img = GetComponent<Image>();
        imgMouse = ImageMouse.mouse.img;

    }
     public void BeginDrag()
    {
       
        ImageMouse.mouse.img.sprite = img.sprite;
        img.color = ImageMouse.mouse.alphaZero;
        imgMouse.color = Color.white;
    }
    public void EndDrag()
    {
        if (imgMouse.sprite)
        {
            img.sprite = imgMouse.sprite;
            img.color = Color.white;
            imgMouse.color = ImageMouse.mouse.alphaZero;
            imgMouse.sprite = null;
        }
        else
        {
            Destroy(gameObject);
        }
    }


}
