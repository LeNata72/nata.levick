﻿using UnityEngine;
using UnityEngine.UI;//(6)

public class ItemInventar : MonoBehaviour
{
    public Image img;//ссылка на имдж подключаем библиотеку 2 строка(5)
    public int count;//количество предметов (7)
    public string nameItem;// названия предметов (8)
    Text countText;//ссылка на текстовый элемент (9)
    MouseInventar mouse;

    void Start()//инициализачия всего (10)
    {
        mouse = MouseInventar.mouse;
        countText = GetComponentInChildren<Text>();//подтягиваем текстовый элемент (11)
        if (img.sprite)//если есть изображение
        {
            countText.text = count.ToString();//выполняем обновление текстового поля
        }
        else//иначе если пусто
        {
            img.color = new Color(1, 1, 1, 0);//цвет стал прозрачным
            countText.text = "";//предмета нет то текст не отображается
            count = 0;// для подстраховки дальше затягиваем компонент в инспекторе(coздаем скрипт на мышь)
        }
    }

    //начало перетягивания
    public void BeginDrag()//любое название
    {
        print("BeginDrag");
        if (img.sprite && !mouse.img.sprite)
        {
            CellToMouse();
        }
    }

    public void EndDrag()
    {
        print("EndDrag");
        if (mouse.img.sprite)
        {
            MouseToCell();
        }
    }

    public void Drop()
    {
        print("Drop");
        if (img.sprite)
        {
            SwapMouseCell();
        }
        else if (mouse.img.sprite)
        {
            MouseToCell();
        }
    }

    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count == 0 ? "" : count.ToString();
    }

    void CellToMouse()
    {
        mouse.img.sprite = img.sprite;
        mouse.img.color = new Color(1, 1, 1, 1);
        mouse.SetCount(count);
        img.color = new Color(1, 1, 1, 0);
        img.sprite = null;
        SetCount(0);
        mouse.nameItem = nameItem;

    }

    void MouseToCell()
    {
        img.sprite = mouse.img.sprite;
        mouse.img.color = new Color(1, 1, 1, 0);
        img.color = new Color(1, 1, 1, 1);
        mouse.img.sprite = null;
        SetCount(mouse.count);
        mouse.SetCount(0);
        nameItem = mouse.nameItem;
    }

    void SwapMouseCell()
    {
        Sprite tempSprite = img.sprite;
        int tempCount = count;
        SetCount(mouse.count);
        mouse.SetCount(tempCount);
        img.sprite = mouse.img.sprite;
        mouse.img.sprite = tempSprite;
        string tempName = nameItem;
        nameItem = mouse.nameItem;
        mouse.nameItem = tempName;
    }
}
