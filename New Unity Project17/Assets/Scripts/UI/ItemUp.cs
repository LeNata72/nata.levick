﻿using UnityEngine;

public class ItemUp : MonoBehaviour
{
    public string nameItem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            InventarSystem.sys.AddToInventar(GetComponent<SpriteRenderer>().sprite, nameItem);
            Destroy(gameObject);
        }
    }

}
