﻿using UnityEngine;

public class InventarSystem : MonoBehaviour
{
    public static InventarSystem sys;//переменная 1
    public ItemInventar[] elemInv;//массив с сылкой на каждый предмет в инвентаре 3
    public void Awake()
    {
        sys = this;//инициализация переменной 2
        elemInv = GetComponentsInChildren<ItemInventar>();//инициализация массива вернуть компоненты из дочернего 4 дальше в ItemInv...
    }

    public void AddToInventar(Sprite sprite, string nameItem)
    {
        int countElemInv = elemInv.Length;
        for (int i = 0; i < countElemInv; i++)
        {
            if (elemInv[i].count > 0 && elemInv[i].nameItem == nameItem)
            {
                elemInv[i].SetCount(elemInv[i].count + 1);
                return;
            }
        }

        for (int i = 0; i < countElemInv; i++)
        {
            if (!elemInv[i].img.sprite)
            {
                elemInv[i].img.sprite = sprite;
                elemInv[i].SetCount(1);
                elemInv[i].nameItem = nameItem;
                elemInv[i].img.color = new Color(1, 1, 1, 1);
                return;
            }
        }
    }
}
