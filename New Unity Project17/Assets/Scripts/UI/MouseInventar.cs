﻿using UnityEngine;
using UnityEngine.UI;


public class MouseInventar : MonoBehaviour
{
    public static MouseInventar mouse;//1
    public Image img;//2
    public Text countText;//3
    public int count;//4 количество предметов кот перетягиваем
    public string nameItem;//5 название предметов

    void Awake()
    {
        mouse = this;//2
        img = GetComponent<Image>();//3
        countText = GetComponentInChildren<Text>();//4
        img.color = new Color(1, 1, 1, 0);//5 иконка мышки прозрачная 
        countText.text = "";//чтобы не отображался текст 6

    }

    
    void Update()
    {
        transform.position = Input.mousePosition;//смещение иконки вместе с указателем мыши
    }

    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count == 0 ? "" : count.ToString();

    }
}
