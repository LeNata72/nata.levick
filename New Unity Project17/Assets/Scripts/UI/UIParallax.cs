﻿using UnityEngine;
using UnityEngine.UI;

public class UIParallax : MonoBehaviour
{
    public Image imageHP;
    public Image[] imgDiamond;


    float maxHP = 20;
    Color colorDefault;

    private void Start()
    {
        colorDefault = imgDiamond[0].color;    
    }



    public void InitUI(int hp)
    {
        maxHP = hp;
    }

    public void SetHP(int hp)
    {
        imageHP.fillAmount = hp / maxHP;
    }

    public void SetCountDiamond(int countDiamond)
    {
        for (int i = 1; i < 3; i++)
        {
            if (i <= countDiamond)
            {
                imgDiamond[i - 1].color = Color.white;
            }
            else
            {
                imgDiamond[i].color = colorDefault;
            }
        }
    }
}
