﻿using UnityEngine;
using UnityEngine.UI;


public class ImageMouse : MonoBehaviour
{
    //Vector4 alphaZero = new Vector4 (1, 1, 1, 0);
    public static ImageMouse mouse;
    public Image img;

    public Color alphaZero { get => new Color(1, 1, 1, 0); }
    void Awake()// используется чтобы инициализация была быстрее
    {
        mouse = this;
        img = GetComponent<Image>();
        img.color = alphaZero;

    }

    
    void Update()
    {
        transform.position = Input.mousePosition;  
    }
}
