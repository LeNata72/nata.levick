﻿using UnityEngine;

[ExecuteInEditMode]
public class GUIDemo : MonoBehaviour
{
    public Rect rectGroup = new Rect(0, 0, 220, 310);
    public float valSlider = 3, leftSlider = 1, rightSlider = 10;
    public float valScrol = 1, sizeScrol = 2, leftScrol = 1, rightScrol = 8;
    public Texture texture;
    string textFild1 = "", textFild2 = "";
    string passwordFild1 = "";
    string textArea1 = "";
    bool panel1 = false;
    bool tog1 = false, tog2 = false, tog3 = false;
    int idTool;
    string[] itemsMenu = { "Menu 1", "Menu 2", "Menu 3" };

    public Rect window1 = new Rect(Screen.width / 2 - 125, 60, 250, 200);
    public Rect window2 = new Rect(Screen.width / 2 - 125, 60, 250, 200);
    public GUISkin skin;

    private void OnGUI()
    {
        GUI.skin = skin;
        if (!panel1)
        {
            //создание группы
            GUI.BeginGroup(rectGroup);
            //текстовая метка
            GUI.Label(new Rect(10, 10, 200, 20), $"My screen x = {Screen.width}; y = {Screen.height}");
            //текстовое поле
            textFild1 = GUI.TextField(new Rect(10, 40, 200, 20), textFild1);
            //GUI.TextField(new Rect(10, 70, 200, 20), textFild2);   // поле не можна редагувати
            //поле для пароля
            passwordFild1 = GUI.PasswordField(new Rect(10, 70, 200, 20), passwordFild1, '*');
            //поле для многорядового текста
            textArea1 = GUI.TextArea(new Rect(10, 100, 200, 100), textArea1);
            //завершение группы
            GUI.EndGroup();
            //rectGroup.x = 59;
        }
        else
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200));
            //создание графического контейнера для элементов (меню)
            GUI.Box(new Rect(0, 0, 200, 200), "Menu");
            //слайдер для установки уровня (звук, жизни и тп)
            valSlider = GUI.HorizontalSlider(new Rect(10, 30, 180, 20), valSlider, leftSlider, rightSlider);
            //скрол
            valScrol = GUI.HorizontalScrollbar(new Rect(10, 60, 180, 20), valScrol, sizeScrol, leftScrol, rightScrol);
            //текстура шкалы
            GUI.DrawTexture(new Rect(10, 90, valSlider / rightSlider * 180, 20), texture);
            GUI.EndGroup();
        }
        //кнопка
        if (GUI.Button(new Rect(20, Screen.height - 40, 100, 30), "Click me"))
        {
            panel1 = !panel1;
        }
        //флажки
        tog1 = GUI.Toggle(new Rect(20, Screen.height - 140, 180, 20), tog1, "Toggle1");
        tog2 = GUI.Toggle(new Rect(20, Screen.height - 110, 180, 20), tog2, "Toggle2");
        tog3 = GUI.Toggle(new Rect(20, Screen.height - 80, 180, 20), tog3, "Toggle3");

        //переключатель
        idTool = GUI.Toolbar(new Rect(Screen.width / 2 - 125, 20, 250, 30), idTool, itemsMenu);
        if (idTool == 0)
        {
            //создание окна
            window1 = GUI.Window(0, window1, PaintWindows, "Window 1");
        }
        else if (idTool == 1)
        {
            window2 = GUI.Window(1, window2, PaintWindows, "Window 2");
        }
        else
        {
            GUI.Box(new Rect(Screen.width / 2 - 125, 60, 250, 200), "Menu 3");

        }
    }

    //описание элементов окна
    void PaintWindows(int id)
    {
        if (id == 0)
        {
            //содержание 1-го окна
            textFild1 = GUI.TextField(new Rect(10, 40, 200, 20), textFild1);
        }
        else if (id == 1)
        {
            //содержание 2-го окна
            passwordFild1 = GUI.PasswordField(new Rect(10, 70, 200, 20), passwordFild1, '*');
        }
        //метод для перетягивания окон
        GUI.DragWindow();
    }

}