﻿using UnityEngine;

public class ImageOnOff : MonoBehaviour
{
    public void SetNoOff()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
