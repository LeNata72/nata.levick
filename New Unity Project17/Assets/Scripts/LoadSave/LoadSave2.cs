﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadSave2 : MonoBehaviour
{
    public Save saveGame;
    string path;

    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
       path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
        path = Path.Combine(Application.dataPath, "Sava.json");
#endif

        if (File.Exists(path))
        {
            saveGame = JsonUtility.FromJson<Save>(File.ReadAllText(path));
        }
        else
        {
            print("not file save");
        }

    }

    private void OnApplicationQuit()
    {
        File.WriteAllText(path, JsonUtility.ToJson(saveGame));
    }



}

[System.Serializable]
public class Save
{
    public string userName;
    public int level;
    public int hp;
    public Vector3 lastPositino;
    public Vector3[] checkPoints;
}