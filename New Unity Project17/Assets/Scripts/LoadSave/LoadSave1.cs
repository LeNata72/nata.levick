﻿using UnityEngine;

public class LoadSave1 : MonoBehaviour

{
    public int score = 0;

    public Transform cube;
    public SpriteRenderer star;
    public Transform[] resObjects;
    void Start()
    {
        cube = Resources.Load<Transform>("Cube");
        star = Resources.Load<SpriteRenderer>("obj1/star");
        resObjects = Resources.LoadAll<Transform>("");

        Instantiate(cube, new Vector3(-2, 0, 0), Quaternion.identity);
        Instantiate(star, new Vector3(2, 0, 0), Quaternion.identity);

        if (PlayerPrefs.HasKey("score"))
        {
            score = PlayerPrefs.GetInt("score");
        }
        else
        {
            print("Ключ score в реестре не найден");
        }
        if (PlayerPrefs.HasKey("pos"))
        {
            string strPos = PlayerPrefs.GetString("pos");
            string [] arrayStr = strPos.Split(';');
            Vector3 tempPos = Vector3.zero;
            for (int i = 0; i < 3; i++)
            {
                tempPos[i] = float.Parse(arrayStr[i]);
            }
            transform.position = tempPos;
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("score", score);

        string strPos = "";
        for (int i = 0; i < 3; i++)
        {
            strPos += transform.position[i] + ";";//1: 2; 3;
        }
        strPos = strPos.Remove(strPos.Length - 1);//1; 2; 3
        print(strPos);
        PlayerPrefs.SetString("pos", strPos);
    }
}
