﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray1 : MonoBehaviour
{


    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                print(hit.collider.name);

                Debug.DrawLine(Camera.main.transform.position, hit.point, Color.green, 1);

                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                obj.transform.position = hit.point + Vector3.Scale(hit.normal, hit.transform.localScale / 2f);

            }
        }
        else if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                hit.transform.localScale *= Input.GetAxis("Mous Y") * 0.05f + 1;
                //1*0.03 = 0.03
                //1 * 1.03 = 1.03
                //1* 0.97 = 0.97
            }

        }        
    
    }
}
