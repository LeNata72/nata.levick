﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera1 : MonoBehaviour
{
    public Camera cam1;
    Vector4 rectMax;
    Vector4 rectMin1;
    bool isMax1 = false;

    void Start()
    {
        rectMax = new Vector4(0, 0, 1, 1);
        rectMin1 = new Vector4(0.03f, 0.04f, 0.25f, 0.25f);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isMax1)
            {
                StartCoroutine(AnimCameraMin(cam1, rectMin1));
            }
            else
            {
                StartCoroutine(AnimCameraMax(cam1));
            }
            isMax1 = !isMax1;
        }
    }

    IEnumerator AnimCameraMax(Camera camMax)
    {
        camMax.depth = 1;
        Vector4 rectTemp;
        while (true)
        {
            yield return null;
            rectTemp = new Vector4(camMax.rect.x, camMax.rect.y, camMax.rect.width, camMax.rect.height);
            rectTemp = Vector4.MoveTowards(rectTemp, rectMax, 0.05f);
            camMax.rect = new Rect(rectTemp.x, rectTemp.y, rectTemp.z, rectTemp.w);

            if (Vector4.Distance(rectTemp, rectMax) < 0.001)
            {
                break;
            }
        }
    }

    IEnumerator AnimCameraMin(Camera camMin, Vector4 rectMin)
    {
        camMin.depth = 0;
        Vector4 rectTemp;
        while (true)
        {
            yield return null;
            rectTemp = new Vector4(camMin.rect.x, camMin.rect.y, camMin.rect.width, camMin.rect.height);
            rectTemp = Vector4.MoveTowards(rectTemp, rectMin, 0.05f);
            camMin.rect = new Rect(rectTemp.x, rectTemp.y, rectTemp.z, rectTemp.w);

            if (Vector4.Distance(rectTemp, rectMin) < 0.001)
            {
                break;
            }
        }
    }
}