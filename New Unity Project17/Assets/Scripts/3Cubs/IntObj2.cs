﻿using UnityEngine;
using UnityEngine.UIElements;

public class IntObj2 : MonoBehaviour
{
    public GameObject cube;
    public int n = 4;
    public float delta = 0.1f;
    public float kut = 3;
    GameObject goRoot;
    GameObject cloneCube;

    void Start()
    {
        goRoot = new GameObject("RootCube");
        float center = (n - 1 + (n - 1) * delta) / 2f;
        transform.position = new Vector3(center, center, center);
        for (int x = 0; x < n; x++)
        {
            for (int y = 0; y < n; y++)
            {
                for(int z = 0; z < n; z++)
                {
                    GameObject temp = Instantiate(cube, new Vector3(x + x * delta, y + y * delta, z + z * delta), Quaternion.identity, transform);
                    temp.GetComponent<MeshRenderer>().material.color = new Color(1-x/(n-1f), 1-y/(n-1f),1-z/(n-1f));
                }
            }
        }
    }

    
}
