﻿using UnityEngine;

public class IntObg : MonoBehaviour
{
    public GameObject cube;
    
    public int n = 4;
    public float delta = 0.1f;

    void Start()
    {
        float center = (n - 1 + (n - 1) * delta) / 2f;
        transform.position = new Vector3(center, center, 0);


        for(int x = 0; x < n; x++)
        {
            for(int y = 0; y < n; y++)
            {
                Instantiate(cube, new Vector3(x+x*delta,y+y*delta,0), Quaternion.identity,transform);
            }
        }
    }

    
}
