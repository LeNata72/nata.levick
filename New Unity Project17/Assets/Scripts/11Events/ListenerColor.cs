﻿using UnityEngine;

public class ListenerColor : MonoBehaviour
{
    MeshRenderer ren;
        void Awake()
    {
        ren = GetComponent<MeshRenderer>(); //теперь можено использовать в обробнику подій 
        Messenger<Color>.AddListener(GameEvent.SEND_COLOR, OnSetColor);
    }

    
    void OnDestroy()
    {
        Messenger<Color>.RemoveListener(GameEvent.SEND_COLOR, OnSetColor);

    }
    void OnSetColor(Color col)
    {
        //должна быть ссылка на компонент мэш рендерер реализуем в 5 строке 
        // ren - получаем доступ к компоненту в котором есть материал в материале цвет и єто цвет будет такой как пришел
         ren.material.color = col;
    }
}
