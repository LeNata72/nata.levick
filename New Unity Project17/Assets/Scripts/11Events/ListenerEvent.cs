﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerEvent : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        Messenger.AddListener(GameEvent.SEND_EVENT, OnSendEvent);
        Messenger<string>.AddListener(GameEvent.SEND_MESS, OnSendMess);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.SEND_EVENT, OnSendEvent); 
        Messenger<string>.RemoveListener(GameEvent.SEND_MESS, OnSendMess);

    }
    void OnSendEvent()
    {
        print("OnSendEvent " + name);
    }

    void OnSendMess(string mess)
    {
        Debug.LogFormat("OnSendMess {0} {1}", mess, name);
    }
}
