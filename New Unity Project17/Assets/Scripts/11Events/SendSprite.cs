﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendSprite : MonoBehaviour
{
    SpriteRenderer sprite;
    public Sprite[] sprites;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }


    private void OnMouseDown()
    {
        Sprite newSprite = sprites[Random.Range(0, sprites.Length)];
        sprite.sprite = newSprite;
        Messenger<Sprite>.Broadcast(GameEvent.SEND_SPRITE, newSprite);
    }


}
