﻿using UnityEngine;

public class ListenerSprite : MonoBehaviour
{
    SpriteRenderer sr;
    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        Messenger<Sprite>.AddListener(GameEvent.SEND_SPRITE, OnSetSprite);
    }
    private void OnDestroy()
    {
        Messenger<Sprite>.RemoveListener(GameEvent.SEND_SPRITE, OnSetSprite);
    }

    // Update is called once per frame
    void OnSetSprite (Sprite spr)
    {
        sr.sprite = spr;
    }
}
