﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendColor : MonoBehaviour
{
    MeshRenderer ren;
    void Start()
    {
        ren = GetComponent<MeshRenderer>();
    }

   
    private void OnMouseDown()
    {
        Color randomColor = new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        ren.material.color = randomColor;
        Messenger<Color>.Broadcast(GameEvent.SEND_COLOR, randomColor);
    }
}
