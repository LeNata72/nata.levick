﻿using UnityEngine;

public class SendMessEvent : MonoBehaviour
{
       void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Messenger.Broadcast(GameEvent.SEND_EVENT);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Messenger<string>.Broadcast(GameEvent.SEND_MESS, "Hello");
        }
    }
}
