﻿public static class GameEvent
{
    public const string SEND_EVENT = "SEND_EVENT";
    public const string SEND_MESS = "SEND_MESS";

    public const string SEND_COLOR = "SEND_COLOR";
    public const string SEND_SPRITE = "SEND_SPRITE";
    public const string SEND_NPC = "SEND_NPC";

}
