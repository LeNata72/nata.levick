﻿// Головний скрипт системи пулінга
using System.Collections.Generic;
using UnityEngine;

public class PoolSys : MonoBehaviour
{
    public static PoolSys sys; // статична змінна для швидкого доступу
    public PoolObj[] poolObj; // масив об'єктів що будуть формувати пули
    List<PoolObj>[] pool; // масив списків - набір пулів
    int[] countActiv; // масив для збереження кількості активних об'єктів в кожному пулі окремо
    Transform[] poolParent;  // масив трансформів батьківських об'єктів кожного з пулів окремо

    void Awake()
    {
        sys = this;
        pool = new List<PoolObj>[poolObj.Length];
        poolParent = new Transform[poolObj.Length];

        for (int i = 0; i < pool.Length; i++)
        {
            pool[i] = new List<PoolObj>();

            poolParent[i] = new GameObject().transform;
            poolParent[i].parent = transform;
            poolParent[i].name = poolObj[i].name;
        }

        countActiv = new int[pool.Length];
    }

    // додавання об'єкта в пул
    GameObject AddToPool(int idPool, Vector3 pos)
    {
        PoolObj temp = Instantiate(poolObj[idPool], pos, Quaternion.identity, poolParent[idPool]);
        pool[idPool].Add(temp);
        temp.idPool = idPool;
        return temp.gameObject;
    }

    // отримання об'єкта з пула
    public GameObject TakeFromPool(int idPool, Vector3 pos)
    {
        GameObject temp = null;
        if (countActiv[idPool] < pool[idPool].Count)
        {
            for (int i = 0; i < pool[idPool].Count; i++)
            {
                if (!pool[idPool][i].gameObject.activeSelf)
                {
                    temp = pool[idPool][i].gameObject;
                    temp.transform.position = pos;
                    break;
                }
            }
        }
        if (!temp)
        {
            temp = AddToPool(idPool, pos);
        }

        temp.SetActive(true);
        countActiv[idPool]++;
        return temp;
    }

    // повернення об'єкта в пул
    public void ReturnInPool(int idPool, PoolObj obj)
    {
        countActiv[idPool]--;
        obj.gameObject.SetActive(false);
    }

}