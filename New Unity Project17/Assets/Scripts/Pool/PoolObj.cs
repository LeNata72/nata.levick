﻿// Допоміжний скріпт, знаходиться на кожному об'єкті в пулі
using UnityEngine;

public class PoolObj : MonoBehaviour
{
    public int idPool;

    public void DestroyObj()
    {
        PoolSys.sys.ReturnInPool(idPool, this);
    }
}