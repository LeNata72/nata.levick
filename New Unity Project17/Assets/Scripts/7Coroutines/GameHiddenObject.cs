﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHiddenObject : MonoBehaviour
{
    public List<string> objectsName;//список всех предметов
    public List<string> objectsFind;//список предметов кот нужно найти сейчас
    int score = 0;
    float timerGame = 0;
    float timerTask = 0;
    Coroutine corGame, corTask;


    void Start()
    {
        HiddenObject[] temp = GetComponentsInChildren<HiddenObject>();
        // циклом просматриваем все элементы, наполняем его
        foreach (var item in temp)
        {
            objectsName.Add(item.name); // на основе массива(17) заполняем список предметов (10)
        }
        timerGame = objectsName.Count * 10;// устанавл. время игры на основе количества элементов * на время на кажд элемент
        NextObjectsToFind();
        corGame = StartCoroutine(TimerGame());
        corTask = StartCoroutine(TimerTask());
    }
    void NextObjectsToFind()//генерация новых предметов для новго задания
    {
        timerTask = 0;// таймер на 0 для начала
        if (objectsName.Count == 0) //если предметов не остается игре конец
        {
            GameOver(true);
            return;
        }
        for (int i = 0; i < 3; i++)// генерация новых элементов
        {
            if (objectsName.Count > 0)// перестраховка проверка, если предметов > 0
            {
                int random = Random.Range(0, objectsName.Count);// ищем новый элемент 0-12 выпадет случайный элемент
                objectsFind.Add(objectsName[random]);// к списку добавили новый случаййный элемент
                objectsName.RemoveAt(random);// удаляем предмет который выпал из списка
            }
        }
        PrintObjectsFind();//печатаем объекьты для поиска метод 47
    }
    void PrintObjectsFind()// выводит названия
    {
        string mess = "Знайдіть об’єкти:";
        foreach (var item in objectsFind)//список для поиска
        {
            mess += "\t\t" + item;// название предмета
        }
        print(mess);
    }
    void GameOver(bool isWinner)
    {
        string mess = "Game over, you ";
        if (isWinner)
        {
            mess += "winer\nScore: " + score;
        }
        else
        {
            mess += "Спробуй ще";
        }
        print(mess);
        StopCoroutine(corGame);
        StopCoroutine(corTask);
    }

    IEnumerator TimerGame()// таймер для игры
    {
        while (timerGame > 0)
        {
            yield return null;
            timerGame -= Time.deltaTime;
        }
        GameOver(false);
    }

    IEnumerator TimerTask()
    {
        while (true)
        {
            yield return null;
            timerTask += Time.deltaTime;
        }
    }

    public bool IsFind(string name)
    {
        foreach (var item in objectsFind)
        {
            if (item == name)
                return true;
        }
        return false;
    }

    public void AddScore()
    {
        if (timerTask < 4)
            score += 3;
        else if (timerTask < 12)
            score += 2;
        else
            score++;
        print("Score: " + score + "\nTime left: " + timerGame);
    }

    public void DeleteObject(HiddenObject obj)
    {
        objectsFind.Remove(obj.name);
        Destroy(obj.gameObject);
        if (objectsFind.Count == 0)
        {
            NextObjectsToFind();
        }
    }
}       
    




    
