﻿using UnityEngine;
using System.Collections;
public class CorRotCube : MonoBehaviour
{
    Coroutine cor = null;
    IEnumerator Anim()
    {
        while (true)
        {
            transform.Rotate(30 * Time.deltaTime, 30 * Time.deltaTime, 0);
            yield return null;
        }
    }
    private void OnMouseEnter()
    {
        if(cor == null)
          cor = StartCoroutine(Anim());
    }
    private void OnMouseExit()
    {
        StopCoroutine(cor);
        cor = null;
    }
}
