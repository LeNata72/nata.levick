﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoCoroutine : MonoBehaviour
{
    public Transform obj;
    void Start()
    {
        StartCoroutine(Timer());
        StartCoroutine(Anim());
    }

    IEnumerator Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            print("Hello");
            yield return new WaitForSeconds(1);
            print("Unity");

        }
    }
    IEnumerator Anim()
    {
        while (true)
        {
            obj.Rotate(30 * Time.deltaTime, 30 * Time.deltaTime, 0);
            yield return null;
        }
    }
}
