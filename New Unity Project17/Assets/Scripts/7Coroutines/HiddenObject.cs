﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObject : MonoBehaviour
{
    GameHiddenObject game;
    SpriteRenderer sr;

    void Start()
    {
        game = GetComponentInParent<GameHiddenObject>();
        sr = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        if (game.IsFind(name))
            StartCoroutine(YesFind());

    }

    IEnumerator YesFind()
    {
        game.AddScore();
        for (int i = 0; i < 3; i++)
        {
            sr.color = Color.green;
            yield return new WaitForSeconds(0.2f);
            sr.color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
        game.DeleteObject(this);
    }

   

}
