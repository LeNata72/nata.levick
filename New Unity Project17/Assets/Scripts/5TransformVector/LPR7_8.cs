﻿using UnityEditor.SceneManagement;
using UnityEngine;

public class LPR7_8 : MonoBehaviour
{
    void Start()
    {
        GameObject go1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go1.transform.position = Vector3.zero;
        go1.name = "obj 1";

        GameObject[] go1Array = new GameObject[3];
        for (int i = 1; i <= 3; i++)
        {
           go1Array[i-1] = AddObject(new Vector3(-20 + (10*i), -3, 0), "obj 1" + i, go1.transform);
        }

        for (int i = 1; i <= 3; i++)
        {
             AddObject(new Vector3(-14 + (2 * i), -6, 0), "obj 1.1" + i, go1Array[0].transform);
        }

        for (int i = 1; i <= 4; i++)
        {
           AddObject(new Vector3(-5 + (2 * i), -6, 0), "obj 1.2" + i, go1Array[1].transform);
        }

        GameObject obj131 = AddObject(new Vector3(10, -6, 0), "obj 1.3.1", go1Array[2].transform);


        GameObject obj1311 = AddObject(new Vector3(10, -9, 0), "obj 1.3.1.1", obj131.transform);
        for(int i = 1; i <= 2; i++)
        {
            AddObject(new Vector3(7 + (2*i), -12, 0), "obj 1.3.1.1.1" + i, obj1311.transform);
        }

    }
    GameObject AddObject(Vector3 pos, string name, Transform parent)

    {
            GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            newObj.transform.position = pos;
            newObj.name = name;
            newObj.transform.parent = parent;
            return newObj;
    }
    
  
}
