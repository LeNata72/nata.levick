﻿using UnityEngine;

public class DemoPatrul : MonoBehaviour
{
    public Transform target;
    public float speed = 1;
    public float distanceStop = 3;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        if(Vector3.Distance(transform.position, target.position) > distanceStop)
        {
             transform.Translate(Vector3.forward * Time.deltaTime, Space.Self);
        }
    }
}
