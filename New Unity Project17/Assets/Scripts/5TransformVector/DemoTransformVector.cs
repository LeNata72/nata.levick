﻿using UnityEngine;

public class DemoTransformVector : MonoBehaviour
{
    public Vector2 v2;
    public Vector3 v3;
    public Vector4 v4;
    public Transform obj1;
    public Transform obj2;
    public float speedRot = 50;
    public float deltaY = 0.5f;
    public float speedY = 30;
    float maxY;
    float minY;
    float t = 0.5f;
    bool isUp = true;

    // Start is called before the first frame update
    void Start()
    {
        v2 = Vector2.one;
        v2 = new Vector2(1, 1);

        Vector2 a = new Vector2(1, 2);
        Vector2 b = new Vector2(4, 4);

        /*for (int t = 0; t <= 10; t++)
            print(Vector2.Lerp(a, b, t / 10f));*/

        /* while (Vector2.Distance(a, b) > 0.01)
         {
             a = Vector2.MoveTowards(a, b, 0.3f);
             print(a);
         }*/

        maxY = obj2.position.y + deltaY;
        minY = obj2.position.y - deltaY;
    }

    // Update is called once per frame
    void Update()
    {
        obj2.RotateAround(obj1.position, Vector3.up, speedRot*Time.deltaTime);

        if (isUp)
            t += speedY;
        else
            t -= speedY;

        Vector3 pos = obj2.transform.position;

        pos = new Vector3(pos.x, Mathf.Lerp(minY, maxY, t), pos.z);
        obj2.transform.position = pos;
        if (t > 1 && isUp)
        {
            isUp = false;
        }
        else if(t < 0 && !isUp)
        {
            isUp = true;
        }
    }
}
