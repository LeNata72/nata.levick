﻿using UnityEngine;

public class CameraDemo : MonoBehaviour
{
    public Transform[] posPath;
    public float speed = 1;
    ParallaxDemo parallax;
    int idNextPos = 1;
    float lastPosX;

    private void Awake()
    {
        parallax = GetComponentInChildren<ParallaxDemo>();
        lastPosX = transform.position.x;
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, posPath[idNextPos].position) > 0.01)
        {
            transform.position = Vector3.MoveTowards(transform.position, posPath[idNextPos].position, speed * Time.deltaTime);
        }
        else
        {
            idNextPos = ++idNextPos % posPath.Length;
        }
        parallax.run = (transform.position.x - lastPosX) * 10;
        lastPosX = transform.position.x;
    }
}
