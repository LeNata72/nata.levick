﻿using UnityEngine;

public class ParallaxDemo : MonoBehaviour
{
    public Transform[] parArray;
    public float run;

    void Update()
    {
        parArray[0].localPosition -= new Vector3(0.01f * run, 0, 0);
        parArray[1].localPosition -= new Vector3(0.01f * run, 0, 0);
        parArray[2].localPosition -= new Vector3(0.02f * run, 0, 0);
        parArray[3].localPosition -= new Vector3(0.02f * run, 0, 0);
        parArray[4].localPosition -= new Vector3(0.04f * run, 0, 0);
        parArray[5].localPosition -= new Vector3(0.04f * run, 0, 0);

        for (int i = 0; i < 6; i++)
        {
            if (parArray[i].localPosition.x < -25)
                parArray[i].localPosition += new Vector3(50, 0, 0);
            else if (parArray[i].localPosition.x > 25)
                parArray[i].localPosition -= new Vector3(50, 0, 0);
        }
    }
}
